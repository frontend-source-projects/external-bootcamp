# Docker

## Installation

- Enable VT-x at BIOS
- (on windows) Enable Hyper-V windows service

## Commands

### Container Build

Powershell:

```powershell
docker build -f .\Dockerfile -t app .
$CONTAINER_ID = docker run -d app
```

Bash:

```bash
docker build -f ./Dockerfile -t app .
CONTAINER_ID=$(docker run -d app)
```

### Container Management

**Log standard output from container**

```
docker logs $CONTAINER_ID --follow
```

**Access the container**

```
docker exec -it $CONTAINER_ID /bin/bash
```

**Remove the container**

```
docker stop $CONTAINER_ID ; docker rm $CONTAINER_ID
```

**Get IP address of container**

```
docker inspect $CONTAINER_ID | jq .[0].NetworkSettings.IPAddress
```

**Cleanup unused containers and cache**

```
docker system prune -a
```

## References

- [docker](https://docs.docker.com/)
