# Semantic Versioning

![semver](/images/semver.png)

Semantic Versioning (SemVer for short) is a standardized way to give meaning to
your software releases.

Given a version number `MAJOR.MINOR.PATCH`:

- Increment the `MAJOR` version when you make incompatible API changes
- Increment the `MINOR` version when you add functionality in a
  backwards-compatible manner, and
- Increment the `PATCH` version when you make backwards-compatible bug fixes

`MINOR`, `MAJOR` and `PATCH` are called version segments. An alternative, more
descriptive way to name the segments is `BREAKING.FEATURE.FIX`

## Version ranges

Node.js community uses SemVer and all packages under the [NPM registry](https://registry.npmjs.org/)
are expected to follow the SemVer specification

To get backwards compatible updates (bugfixes and new features) for the
libraries used in the project, `package.json` lets us specify a version range
for our libraries

The version range comes in several formats, such as: hyphen ranges, wildcard
ranges, tilde and caret ranges. For a full specification of available version
ranges visit the [semver package documentation](https://docs.npmjs.com/misc/semver#ranges)

The most used ones (tilde and caret ranges) are described here

### Tilde (`~`) version ranges

A tilde before the version number will allow increments of the FIX version, but
not to the BREAKING or FEATURE versions

`~1.2.3` is equivalent to `=1.2.3 <1.3.0`

### Caret (`^`) version ranges

A caret will allow increments of both the FIX and FEATURE versions

`^1.2.3` is equivalent to `>=1.2.3 <2.0.0`

## References

- <https://semver.org/>
- <http://www.jontejada.com/blog/galvanize/talk/2016/01/18/semver/>
- <https://www.jering.tech/articles/semantic-versioning-in-practice>
