# Git Hooks

## What are Git Hooks?

Git hooks are scripts that Git executes before or after events such as:
`commit`, `push`, and `receive`.

Every Git repository has a `.git/hooks/` folder with a script for each hook you
can bind to. You're free to change or update these scripts as necessary, and Git
will execute them when those events occur.

To implement a git hook, overwrite (or create) one of the scripts in
`.git/hooks` and make it executable.

Hook scripts can be implemented using any language. `shell`, `ruby`, `perl` and
`python` are the scripts are the most common.

The language of the script is determined by the shebang notation
(`#!<interpreter>`, e.g. `#!/bin/sh` for `shell` interpreter) as it is
usually in Linux based software. Note that this also applies to Windows because
`Git for Windows` runs under `MSYS` (a linux emulation system).

## Use cases

We want the test suite to run automatically every time we change something, so
we know things aren't broken.

We also want to make sure that our test coverage is high enough, because
untested code won't break the build but it will surely break your production
environment.

These are good examples of common hooks that you should implement in order to
decrease build breakage.

### `pre-commit` hook example

To perform those kind of code checks **before commiting** we could proceed as
follows:

1.  Edit the `pre-commit` hook
2.  Add the commands we want to use (e.g. `npm run lint` or `npm test`)
3.  Save file
4.  Make file executable (`chmod +x <path/to/file>`)

`.git/hooks/pre-commit` example:

```sh
#!/bin/sh
npm test
```

Having the previous file as executable, when we perform a `git commit`, Git will
execute the `pre-commit` hook before commiting.

- If the script terminates with a zero status code (meaning that `npm test`
  succeed), the commit will be done
- Otherwise, if the script terminates with a non-zero status code (meaning
  that `npm test` failed), the commit will be rejected

## Bypass failing hooks

If you supply the `--no-verify` argument to the git command then the command
will still be executed even if one (or more) of the hooks failed (by returning
a non-zero status code). **Please do this only if you know what you are doing**.

```
git commit --no-verify
```

## Husky

Husky is a git hooks manager for Node.js. It allows to define project hooks in
the `package.json` instead of editing `shell` files in `.git/hooks/` directory.

### Builtin Pre-Push hook

Both [Angular](https://gitlab.com/d4i-frontend-team/angular-starter) and
[React](https://gitlab.com/d4i-frontend-team/react-starter) starters define a
`pre-push` hook by default to minimize bugs being pushed to the remote
repository.

The `pre-push` hook has been chosen over a `pre-commit` one to allow a faster
development iteration.

In your local environment, you can commit code with linter or test errors, as
well as work in progress development.

Once you are done with development and your code is free from linter and test
errors, you are ready to push your changes to the remote repository.

The built-in `pre-push` hook performs the following checks before allowing you
to push your changes:

1. `npm run check-uncommited` checks that there are no pending changes to be
   commited
2. `npm run lint` checks that all your source files are free of linter errors
3. `npm test` checks that all unit tests passed without errors

If any check fails, the push is denied.

```json
{
  "scripts": {
    "test": "npm run lint && npm run test-app",
    "check-uncommitted": "git diff --quiet && git diff --cached --quiet"
  },
  "husky": {
    "hooks": {
      "pre-push": "npm run check-uncommitted && npm test"
    }
  }
}
```

## References

- <https://githooks.com/>
- <http://omerkatz.com/blog/2013/2/15/git-hooks-part-1-the-basics>
- <https://git-scm.com/docs/githooks>
- <https://github.com/aitemr/awesome-git-hooks>
- <https://github.com/typicode/husky>
