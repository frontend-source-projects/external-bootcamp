# Changelog convention

All projects should include a `CHANGELOG.md` file at its root directory
reflecting all changes included on each version of the project.

A changelog is useful to make it easier for users and contributors to see
precisely what notable changes have been made between each release (or version)
of the project.

This changelog convention is based on the
[Keep a Changelog](https://keepachangelog.com/en/1.0.0/) format.

You can see example implementations in both the
[Angular](https://gitlab.com/d4i-frontend-team/angular-starter) and
[React](https://gitlab.com/d4i-frontend-team/react-starter) starters.

## Guiding Principles

- Changelogs are for humans, not machines.
- There should be an entry for every single version.
- The same types of changes should be grouped.
- Versions and sections should be linkable.
- The latest version comes first.
- The release date of each version is displayed.

## Types of changes

- `Added` for new features.
- `Changed` for changes in existing functionality.
- `Deprecated` for soon-to-be removed features.
- `Removed` for now removed features.
- `Fixed` for any bug fixes.
- `Security` in case of vulnerabilities.

## References

- <https://keepachangelog.com/>
