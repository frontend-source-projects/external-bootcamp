# BEM: How to name and structure your CSS clases



## But first, why worrying about name clases?

Writing CSS is easy. But writing CSS that is **maintainable, understandable and scalable** is a completely different thing. As front-end developers we spend a lot of time writing and maintaining CSS code. It may be hard when there are anidated styles that affect more than one section or component in the website, not mentioning specificity issues. One doesn't know what can be edited or deleted and this may end up with the abuse of !importants, overwriting styles over and over again and adding useless extra selectors.

> "There are a number of common problems when working with CSS at scale, but the major two are **clarity and confidence:**
>
> - **Clarity**: How much information can we glean from the smallest possible source? Is our code self-documenting? Can we make safe assumptions from a single context? How much do we have to rely on external or supplementary information in order to learn about a system?
> - **Confidence**: Do we know enough about our code to be able to confidently make changes? Do we have a way of knowing the potential side effects of making a change? Do we have a way of knowing what we might be able to remove?
>
> *Harry Roberts, creator of ITCSS*



## So, what is BEM?

BEM is a methodology for naming CSS styles. According to [Getbem](http://getbem.com/), BEM is a highly useful, powerful, and simple naming convention that makes your front-end code easier to read and understand, easier to work with, easier to scale, more robust and explicit, and a lot more strict.

![alt text](https://miro.medium.com/max/638/1*xHJeNZlLtdFkjMMIL1z4Ag.jpeg)



## Writing BEM

BEM stands for **Block - Element - Modifier**. 



#### **B - Block**:

`blockName`

A block is a a standalone entity that has meaning on its own.

```CSS
//Syntax

.card {
	width: 300rem;
    height: 500rem;
}
```



#### **E - Element:**

`blockName__elementName`

Elements are children of a block. They have no standalone meaning and are semantically tied to a block. Elements are prefixed with their block name and a double underscore.

```CSS
//Example

.card__title {
	*css rules*
}

.card__text {
    font-size: 16px;
}

.card__button {
	*css rules*
}
```



#### **M - Modifier:**

`blockName--modifierName` `blocKName__elementName--modifierName`

Modifiers are used to change the appearence of a block or an element. Use them to change styles or behaviours. 

```CSS
//Example

.card__text--secondary {
    font-size: 14px;
}
```



Here you can see how this is reflected on the card:

![alt text](https://miro.medium.com/max/2390/1*tBFD64u6_kmPVFNxjau17A.png)



## How to use BEM with Saas
![alt text](https://miro.medium.com/max/1280/1*WgtBZtEa4jwvcQH-BEsKjA.jpeg)

**One of the small but incredibly powerful features you get in SASS, is the ability to nest your class selectors**
We are going to use BEM with Sass to generate the class names easily. So if we write in our Sass document:

Let's see how it would work for a CTA block:
```HTML
<div class="cta">
  <h3 class="cta__title">Want to know more?</h3>
  <button class="cta__button cta__button--blue">Click here</button>
</div>
```

```SCSS
.cta {
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;
  padding: 3rem;
  background-color: Seashell;
  font-family: Helvetica, sans-serif;
  
  
  &__title {
    font-size: 20px;
  }
  
  &__button {
    padding: 15px;
    border: none;
    background-color: MediumVioletRed;
    font-size: 16px;
    text-transform: uppercase;
    
    &--blue {
      background-color: MediumSlateBlue;	
    }
  }
}
```

It transpiles out like this:

```CSS
.cta {
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;
  padding: 3rem;
  background-color: Seashell;
  font-family: Helvetica, sans-serif;
}
.cta__title {
  font-size: 20px;
}

.cta__button {
    padding: 15px;
    border: none;
    background-color: MediumVioletRed;
    font-size: 16px;
    text-transform: uppercase;
}
.cta__button--blue {
    background-color: MediumSlateBlue;
}
```


## Good practices & Pro tips

### 1) Avoid the "grandchildren" pattern.

Avoid having classes with `blockname__parentElementName__childElementName`. BEM helps us identify relationships with the top level component block and their "child" clases, but **we don't have to follow strictly the levels of our html structure.**

**Don't**:

```HTML
<div class="card">
    <div class="card__header">
        <h2 class="card__header__title">Title text here</h2>
    </div>
    <div class="card__body">
        <img class="card__body__img" src="some-img.png">
        <p class="card__body__text">Lorem ipsum dolor sit amet, consectetur</p>
        <p class="card__body__text">Adipiscing elit.
            <a href="#" class="card__body__text__link">Pellentesque amet</a>
        </p>
    </div>
</div>
```

**Do:**

```SCSS
<div class="card">
    <div class="card__header">
        <h2 class="card__title">Title text here</h2>
    </div>
    <div class="card__body">
        <img class="card__img" src="some-img.png">
        <p class="card__text">Lorem ipsum dolor sit amet, consectetur</p>
        <p class="card__text">Adipiscing elit.
            <a href="#" class="card__link">Pellentesque amet</a>
        </p>
    </div>
</div>
```

[^]: Example from https://medium.com/fed-or-dead/battling-bem-5-common-problems-and-how-to-avoid-them-5bbd23dee319



#### 1.1) Keep nesting levels under 3

Naming your classes good is going to help you keep the nesting levels under control.

**Don't**:

```SCSS
.card {
    &__header{
        &__title {
        }
    }
    &__body{
        &__img{
        }
        &__text{
            &__link{
                &--modifier {
                }
            }
        }
    }
}
```

**Do: **

```SCSS
.card {
    &__header{
    }
    &__title {
	}
    &__body {
	}
	&__img {
    }
    &__text {
    }
    &__link {
        &--modifier{
        }
    }
}
```



### 2) Elements first modifiers later

To keep a good flow of specifity in the stylesheet, write your `.blockName__elementName` first and `.blockName--modifierName` later.

**Don't**

```SCSS
.nav {
//rules come here
	&--dark {
	//rules come here
	}
	&__title {
	//rules come here
		&--dark{
			//rules come here
		}
	}
}
```

**Do**: Keep order

```SCSS
.nav {
//rules come here
	&__title {
	//rules come here
		&--dark{
			//rules come here
		}
	}
	&--dark {
	//rules come here
	}
}
```

### 3) Use CamelCase for composed names

Use _ and - only for distinguishing elements and modifiers to improve readablity.


**Don't**

```HTML
<div class="card">
    <p class="main-text__display-text">Title</h2>
    <p class="main-text__display-text main-text__display-text--dark">Title</h2>
</div>
```


**Do**
```HTML
<div class="card">
    <h2 class="mainText__displayText">Title</h2>
    <p class="mainText__displayText mainText__displayText--dark">Title</h2>
</div>
```


## Pros & Cons

### BEM advantatges

> While 100% predictable code may never be possible, it's important to understand the trade-offs you make with the conventions you choose. If you follow strict BEM conventions, you will be able to update and add to your CSS in the future with the full confidence that your changes will not have side effects.
>
> *Philip Walton*

- **A declarative syntax**: it helps developers understand which clases are responsible for what and how they depend on one another. Designers and developers can consistently between team members.

- Keeps a **low specificity**: BEM avoids CSS conflicts by using unique contextual class names for every block, element and modifier combination.

- **Better CSS perfomance**: Browsers evaluate CSS selectors right to left and BEM encourages frontend developers to create a flat CSS structure with **no nested selectors**. So the less browsers have to evaluate, the faster they can render.

- **Reusability**: By avoiding use of HTML element names in CSS selectors, BEM makes CSS code readily portable to a different HTML structure.

- **Ease of code maintenance**: BEM's modular approach encourages developing independent modules of code and hence easier to maintain & update without affecting other modules in the project.

  

### BEM disadvantatges

As everything, using BEM has some disadvantatges:

- File size increases due to longuer CSS class names
- HTML may 'look' ugly with BEM class names.



## Extra Resources & Fonts

http://getbem.com/

https://www.smashingmagazine.com/2018/06/bem-for-beginners/

https://css-tricks.com/bem-101/

https://medium.com/@arnost/5-tips-to-get-started-with-bem-in-sass-the-right-way-7f7fcee24254

https://medium.com/fed-or-dead/battling-bem-5-common-problems-and-how-to-avoid-them-5bbd23dee31

https://medium.com/fed-or-dead/battling-bem-5-common-problems-and-how-to-avoid-them-5bbd23dee319