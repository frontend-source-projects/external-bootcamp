# How to order CSS declaration blocks



## Why do we need an order?

The goal is to be able to scan the declaration block and understand quickly the essence of the styling. Keeping a coherent order is going to help us find quickly what we are looking for and understand how the elements are laid out at first glance.



## Organizing declarations 

We are going to follow the "order by type" strategy.

### General tips
- Group properties that share the same area of impact.
- Start with big-picture properties which impact stuff outside the element and work in towards the finer details (for example: text is laid out in boxes, then words, then glyps, so properties for font-size and line-height come first, then text-"", then word-"").
- If you need to define separate properties always go clockwise: top, right, bottom, left.



### Defined order

**Sass inheritance**

```
@extend 
@mixin
```

**Generated content**

```scss
Content
```

**Position, Layout & Visibility**

```
position
z-index
top
right
bottom
left
display 
display properties
opacity
transform
```

**Box model**

```
width
height
box-sizing
margin
box-shadow
border
border-radius
padding
```

**Clipping**

```
overflow
clip
```

**Background & Cursor**

```
background
cursor
```

**Typography**

```
font-family
font-size
line-height
font-weight
font-style
text-align
text-transform
letter-spacing
color
```

**Animation**

```
animation
transition
```

**Pseudo-classes & pseudo-elements (nested-rules)**

```
:hover
:focus
:active
:before
:after
:first-child
:last-child
```



### Complete example

Let's set the styles for a floating button. For demo purposes, let's imagine that we only have this button on our website so we have to define everything at this point. 

See result -> https://codepen.io/irene_mallafre/pen/bGdvROa?editors=1100

```scss
.button-sticky {
  /*As we don't have any sass inheritance, We start setting out the position and layout */
  position: absolute;
  z-index: 10;
  bottom: 4rem;
  right: 4rem;
  display: flex;
  align-items: center;
  justify-content: space-around;
  
  /*We define the box model attributes. In this case width and height are set by the content itself */
  box-sizing: border-box;
  box-shadow: 2px 2px 12px 0px rgba(50, 50, 50, 0.41);;
  -webkit-box-shadow: 2px 2px 12px 0px rgba(50, 50, 50, 0.41);;
  -moz-box-shadow: 2px 2px 12px 0px rgba(50, 50, 50, 0.41);;
  border: none;
  border-radius: 100rem;
  padding: 0.5rem 1rem;
  
  /* We don't have clipping values (overflow/clip) */
  
  /* Setting background and cursor behaviour */
  background-color: rgb(102, 153, 255);
  cursor: pointer;
  
   /* Setting text-attributes. */
  font-family: 'Roboto', sans-serif;
  font-size: 0.9rem;
  line-height: 1.1rem;
  font-weight: 700;
  text-align: center;
  text-transform: uppercase;
  letter-spacing: 0.1rem;
  color: rgb(0,0,0);
 
  /* Animation settings */
  transition: all 0.3s ease-in;
  
  /* adding pseudo-states */
  &:hover {
    /* We follow the same order inside*/
      
    /* box-model attributes */
    box-shadow: 2px 2px 12px 0px rgba(50, 50, 50, 0.51);
    -webkit-box-shadow: 2px 2px 12px 0px rgba(50, 50, 50, 0.51);
    -moz-box-shadow: 2px 2px 12px 0px rgba(50, 50, 50, 0.51);
      
    /* background&cursor */
    background-color: lighten(rgb(102, 153, 255), 3%);
  }
}
```
