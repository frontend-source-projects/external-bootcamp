# Mediaqueries

Media queries are the key to responsive design. In this document you are going to find how we work with mediaqueries inside D4I, to craft high-quality responsive interfaces.

Note: Examples has been taken out from [bradfost post](https://bradfrost.com/blog/post/7-habits-of-highly-effective-media-queries)


## Which has to be the value of our mediaqueries?

> Don’t use popular device dimensions (320px = iPhone portrait, 480px = iPhone landscape, 768px iPad portrait, etc) to determine breakpoints. The device landscape is always changing, so today’s values might be moot even just a year down the road. **The Web is inherently fluid,** so it’s our job to create interfaces that look and function beautifully on any screen instead of in just a few arbitrary buckets. –Brad Frost

Sometimes when we receive a design, the designers or PO's are going to specify the desired mediaqueries to work with. This can be based on a particular market research to understand their users and fit to their particular necesities. If that's the case, it is ok to follow the provided mediaqueries but we, as developers, have to be aware and **detect if the design is going to break at some point and propose an improvement** if needed.


## Follow "mobile-first" philoshopy

> Start with the small screen first, then expand until it looks like shit. Time for a breakpoint! –Stephen Hay

If mediaqueries are not provided by the client, the sentence above has to be your guide in order to define which mediaqueries values you need for your project. **We are going to facilitate you a set of values to start** but remember that in case that is not working for your project you can always propose an improvement and consensuate that with your team. 

Whatever the case it’s important to author our styles in a mobile-first manner.

````CSS
// Bad -> desktop first
.column {
    display: flex;
    flex-direction: row;
    width: 50%
}

@media all and (max-width: 50em) {
    .column {
        display: flex;
        flex-direction: column;
        width: 100%
    }
}


// Good -> mobile first. We only introduce layout-specific rules when we need them. 

/*Mobile syles go here if needed */

@media all and (max-width: 50em) {
    .column {
        display: flex;
        flex-direction: column;
        width: 100%
    }
}
````
### Why we should work mobile first?
-> Smaller, simpler, more maintainable code.
-> Provides support for older mobile browsers that don't support media queries.


## Mediaqueries units: use relative units (em)

-> Using relative units for mediaqueries ensures a great accesibility.
-> If we use pixels to declare our breakpoints, page zooming enacts the horizontal scrollbar, and that doesn't generates a good user experience.
-> Declaring breakpoints in relative units allows the browser to adjust the design based on the user zoom level, making a more pleasant and accesible experience.

````CSS
// Bad -> pixel based mediaqueries

@media all and (min-width:800px) {}

// Good -> em based media queries

@media all and (min-width:50em) {}

````


## How to use mediaqueries


**1. Define in your variables page the values for your mediaqueries.**

As we don't focus on specific dispositives, we have to get rid of the habit of naming the mediaqueries with device names (like: tablet, ipad, desktop...). Note that some "tablet" devices have now bigger resolution that desktop computers to mention one example.

````SCSS

$bp-small: 24em;
$bp-med: 46.8em;
$bp-large: 73em;
$bp-xlarge: 85.5em;

````


**2. Create a conditional mixin to use it through all the project**

````SCSS
@mixin breakpoint($point){
    @if $point == bp-small {
        @media (min-width: $bp-small) {
            @content;
        }
    } @else if $point == bp-med {
        @media(min-width: $bp-med) {
            @content;
        }
    } @else if $point == bp-large {
        @media(min-width: $bp-large) {
            @content;
        }
    } @else if $point == bp-xlarge {
        @media(min-width: $bp-xlarge){
            @content;
        }
    }
}
````

**3. Set all your .scss files following the mobile-first pattern. Use only the breakpoints that you need inside each class, so the design doesn't break.**

````SCSS
//Bad -> create mediaqueries blocks
.container {
    font-family: Arial;
    background-color: black;
    width: 100%;

    &__list {
        margin-right: 0;

    }
}

@include breakpoint(bp-xlarge) {
    .container {
    width: 50%;

    &__list {
        margin-right: 3rem;
    }
}


//Good -> define medias inside each class

.container {
    font-family: Arial;
    background-color: black;
    width: 100%;

    @include breakpoint(bp-xlarge) {
        width: 50%;

    &__list {
        margin-right: 0;

        @include breakpoint(bp-xlarge) {
        margin-right: 3rem;
    }
}
````



**4. Test your design and stablish any additional breakpoint you need to ensure that your design doesn't break at any point.**