# Layout best practices

The best practices for CSS layout that we should implement on our websites

## CSS web layout model

### Flexbox

When we received the project designs, an analysis was made and it was decided to use flexbox.

One of the great advantages of Flexbox is its support. Almost all browsers have implemented most of the tool's functionalities. However, 100% compatibility is missing, because so far it supports 98.74%.

![alt-text](https://i.ibb.co/RHfnYDW/compatibility-flexbox.png 'Can I use - flexbox browser support')

In fact, 96.92% of users worldwide use a browser that supports Flexbox. The only problem is support for Internet Explorer (IE) as IE10 and IE11 have only partially implemented Flexbox.
[+info](https://css-tricks.com/snippets/css/a-guide-to-flexbox/)

## Sass Guidelines

### Methodology

- #### Bem
  BEM is a highly useful, powerful, and simple naming convention that makes your front-end code easier to read and understand, easier to work with, easier to scale, more robust and explicit, and a lot more strict.
  [+info](https://css-tricks.com/bem-101/)

### File structure

```
[-] src/
 |- [-] styles/
 |   |– _commons.scss
 |   |– _fonts.scss
 |   |– _mixins.scss
 |   |– _modules.scss
 |   |– _variables.scss
 |   |– _main.scss
```

- `_commons.scss`: Contains standard styles for commonly used HTML elements.
- `_fonts.scss`: Contains the imports of the types of fonts to be used.
- `_mixins.scss`: Contains all the mixins.
- `_modules.scss`: Contains all CSS files from external libraries and frameworks.
- `_variables.scss`: It contains all the variables, each variable has to start with \$.
- `main.scss`: This file should not contain anything but @import and comments.
  Files should be imported according to the folder they live in
  - Mixins
  - Fonts
  - Global Variables
  - Global styles

## Responsive

### Mixin breakpoints

To make the breakpoints of the web a mixin was created to be able to identify the css for different devices in a simple way.

In this case we only needed 3 (mobile, tablet, desktop)

```css
@mixin for-size($size) {
  @if $size==tablet {
    @media (min-width: 768px) {
      @content;
    }
  } @else if $size==desktop {
    @media (min-width: 1440px) {
      @content;
    }
  }
}
```

Since we are mobile first, we always start with the layout for mobile and we don't need to call the mixin. This mixin is only used for tablet and desktop. To use it, it's done in the following way:

```css
.selector {
  // css code for mobile

  @include for-size(tablet) {
    // css code for tablet
  }

  @include for-size(desktop) {
    // css code for desktop
  }
}
```

### Picture img

To improve the performance, we will apply in the images the property picture

The HTML `<picture>` element is a container used to specify multiple `<source>` elements and a `<img>` element contained in it to provide versions of an image for different device scenarios. If there are no matches with the `<source>` elements, the file specified in the src attributes of the `<img>` element is used. The selected image is then presented in the space occupied by the `<img>` element.

To select the optimal image, the user agent examines each srcset attribute, media, and type of the source to select the compatible image.

```html
<picture class="selector">
        <source media="(min-width: 1440px)" [srcset]="url.image.desktop"/>
        <source media="(min-width: 768px)" [srcset]="url.image.tablet" />
        <source media="(max-width: 767px)" [srcset]="url.image.mobile" />
        <source [srcset]="url.image.desktop" />
        <img [alt]="url.image.alt" [src]="url.image.desktop" />
</picture>
```

[+info](https://developer.mozilla.org/es/docs/Web/HTML/Elemento/picture)

## Learnings

#### Before creating a component, it is important to do a good analysis of how it should work.

##### Menu mobile & Custom drop-down list

Interactions need to be taken into account when creating the next components:

- Animations when opening and closing the menu.
- If the menu is open, the user cannot scroll in the area outside of the menu.
- If the menu is open, when the user clicks on the browser back button the menu should close.

#### Deleted bootstrap

Another thing to keep in mind is that if we don't use bootstrap we'll have to erase it as it can create conflicts with styles.
