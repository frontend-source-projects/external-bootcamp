# **React Style Guide**

In this guide we define a set of standards rules for coding, formatting and design react projects.

It is based on the [Airbnb Style Guide](https://airbnb.io/javascript/react) with some additions / modifications that we adopt as Front-end line.

### **Why we need a Style Guide?**

* Each developer writes code differently. That’s fine, until we are working on our code.
* As count of developers increase, things get messy when all are working on common codebase.
* Style guides are created so new developers can get up to speed on a code base quickly, and then write code that other developers can understand quickly and easily.

------

### **Table of Contents**

1.  [React Components Style Guide](components.md)
1.  [Services Style Guide](../redux-epics/style-guide.md#services)
1.  [Store Style Guide](../redux-epics/style-guide.md#store)
1.  [Epics Style Guide](../redux-epics/style-guide.md#epics)
1.  [Models Style Guide](../redux-epics/style-guide.md#models)

------
