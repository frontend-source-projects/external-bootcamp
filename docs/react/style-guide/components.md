# React Components Style Guide

In this guide we define a set of standards rules for coding, formatting and design react components.

### **Table of Contents**

1.  [Basic Rules](#basic-rules)
1.  [Organization](#organization)
1.  [Naming](#naming)
1.  [Stylesheets](#stylesheets)
1.  [Types Definitions](#types-definitions)
1.  [Declaration](#declaration)
1.  [Alignment](#alignment)
1.  [Quotes](#quotes)
1.  [Spacing](#spacing)
1.  [Props](#props)
1.  [Refs](#refs)
1.  [Parentheses](#parentheses)
1.  [Tags](#tags)
1.  [Conditionals](#conditionals)
1.  [Iterators](#iterators)
1.  [Methods](#methods)
1.  [Hooks](#hooks)
1.  [State with Arrays](#react-state-with-arrays)
1.  [State with Objects](#react-state-with-objects)
1.  [Typing](#typing)
1.  [Ordering](#ordering)

------

### **Basic Rules**

* Only include one React component per file.
* Always use [JSX syntax](https://reactjs.org/docs/introducing-jsx.html) inside typescript.
  > Why? TypeScript can help us to avoid painful bugs that developers commonly run into when writing JavaScript by type checking the code.

### **Organization**

* Our project components should be separated in at least three directories.

    ```bash
    src/
    ├─ shared/
    |  └─ components/
    |     └─ table /
    |        └─ table.component.tsx
    |
    ├─ commons/
    |    └─ components/
    |       └─ header/
    |          └─ header.component.tsx
    └─ views/
       ├─ home/
       |  ├─ card/
       |  |  └─ card.component.tsx
       |  └─ home.tsx
       └─ login/
          └─login.tsx
    ```

* **`shared/`**

  `shared directory` has those components that can be used in different projects. They normally are generic enough, so they should be free from business logic.

* **`commons/`**

  `common directory` has those components that can be used in different parts of the application. There are recognize for appearing in different views. Usually they are related to the business so it can't be promoted to shared components.

* **`views/`**

  `views directory` has those components that are related to the business and they only have one instance in the application. They can be divided into:

    ```bash
      views/
      ├── foo/
      |    ├── foo-one/
      |    |   └── foo-one.component.tsx
      |    ├── foo-two/
      |    |   └── foo-two.component.tsx
      |    └── foo.tsx
      └── ...
    ```


  * `Root/View components` are the highest level of application's components. They represent the application routes and most times are displayed by a router.
  * They can store state, receive route parameters and dispatch Redux actions when applicable.
  * They are also responsible for handling callbacks and flowing data into children components.
  * Root components are built mostly from the composition of children components with some styles to layout them together.
  * `Children components` are children of root/view components.
  * They can store internal state and provide additional logic, but all actions should be accepted as component callbacks.
  * Stuff like buttons, inputs, labels and all presentational components goes here.
  * This components can also accept functions as props and dispatch events.

### **Naming**

* **Extensions:**
Use [.tsx](https://www.typescriptlang.org/docs/handbook/jsx.html) extension for React components.
* **Filename**:
Use hyphens and lowercase for filenames. `E.g., reservation-card.tsx`
* **Component Naming:**
    * For root components of a directory (`view components`), use the directory name as the filename without `.component.` suffix.

        ```bash
        views/
        └─ reservations/
           └─ reservations.tsx
        ````

    * For non root components (`children components`), use the directory name as the filename with `.component.` suffix.

        ```bash
        views/
        └─ reservations/
           └─ reservations-card/
              └─ reservation-card.component.tsx
        ```

    * For the component name use the filename without the `.component.` suffix.
    For example, `reservation-card.component.jsx` should have a reference name of `ReservationCard`.

        ```
       // bad
       import ReservationCardComponent from './reservation-card/reservation-card.component';

       // good
       import ReservationCard from './reservation-card/reservation-card.component';
        ```
* **Reference Naming:** 
  
  * Use PascalCase for React components and camelCase for their instances.

      ```
      // bad
     import reservationCard from './reservation-card/reservation-card.component';
  
     // good
     import ReservationCard from './reservation-card/reservation-card.component';
  
     // bad
     const ReservationItem = <ReservationCard />;
  
     // good
     const reservationItem = <ReservationCard />;
      ```

* **Props Naming:**

  * Avoid using DOM component prop names for different purposes.

    > Why? People expect props like style and className to mean one specific thing. Varying this API for a subset of your app makes the code less readable and less maintainable, and may cause bugs.

    ```
    // bad
    <MyComponent style="fancy" />

    // bad
    <MyComponent className="fancy" />

    // good
    <MyComponent variant="fancy" />
    ```

  * Use `on<Subject><Event>` convention when naming event handler props.

    ```
    // bad
    <MyComponent change="handleNameChange" />
    
    // bad
    <MyComponent submit="handleButtonSubmit" />

    // good
    <MyComponent onNameChange="handleNameChange" />
    
    // good
    <MyComponent onSubmit="handleButtonSubmit" />
    ```


### **Stylesheets**


  * **Extensions:**
  Use `.scss` extension for stylesheets.

  * **Language**: Always use `SASS` for style components. Don't use CSS-in-JS unless necessary.

  * **Filename**: Use the component name followed by `.component.scss` suffix for stylesheets. `E.g., card.component.scss`


  * **Naming**: Use [BEM convention](../../css/BEM-naming-clases.md) for naming CSS classes with lowerCamelCase.

    ```
    // bad
    .expansion-panel-large {
      ...
    }
   
    // good
    .expansionPanel--large {
      ...
    }
    ```

  * **Conditional classNames**: Use [classnames](https://github.com/JedWatson/classnames) utility for conditionally joining classNames together.
    ```
    // bad
    const isPrimary = true;
    
    <div
        className={[
          'foo',
          `${isPrimary ? 'foo--primary' : ''}`,
        ].join(' ')}
      >

    // good
    const isPrimary = true;

    <div className={classNames('foo', { 'foo--primary': isPrimary })}>
      ...
    </div>
    ```

  * **Organization**:
  Stylesheets should be placed in the same directory where the components are located.

    ```bash
    src/
    ├─ shared/
    |  └─ components/
    |     └─ table /
    |        ├─ table.component.tsx
    |        └─ table.component.scss
    |
    ├─ commons/
    |    └─ components/
    |       └─ header/
    |          ├─ header.component.tsx
    |          └─ header.component.scss
    └─ views/
      ├─ home/
      |  ├─ card/
      |  |  ├─ card.component.tsx
      |  |  └─ card.component.scss
      |  ├─ home.tsx
      |  └─ home.scss
      └─ login/
          ├─ login.tsx
          └─ login.scss
    ```

  * Use [Material-UI](https://material-ui.com/) if you want to use a React Components library.

### **Types Definitions**

* **Extensions:**
Use `.ts` extension for declaration files.

* **Filename**: Use the component name followed by `.component.types.ts` suffix for declaration files. `E.g., card.component.types.ts`

* **Organization**:
Declaration files should be placed in the same directory where the components are located.

  ```bash
  src/
  ├─ shared/
  |  └─ components/
  |     └─ table /
  |        ├─ table.component.tsx
  |        └─ table.component.types.ts
  |
  ├─ commons/
  |    └─ components/
  |       └─ header/
  |          ├─ header.component.tsx
  |          └─ header.component.types.ts
  └─ views/
     ├─ home/
     |  ├─ card/
     |  |  ├─ card.component.tsx
     |  |  └─ card.component.types.ts
     |  ├─ home.tsx
     |  └─ home.types.tsx
     └─ login/
        ├─ login.tsx
        └─ login.types.tsx
  ```

* Use `I` in UpperCamelCase for prefixing interfaces.

  ```
  // bad
  interface user {
    ...
  }

  // bad
  interface iUser {
    ...
  }

  // good
  interface IUser {
    ...
  }
  ```

* Use `T` in UpperCamelCase for prefixing types.

  ```
  // bad
  type buttonSizes = 'small' | 'medium' | 'large' | 'fullwidth' | 'square';

  // good
  type TButtonSizes = 'small' | 'medium' | 'large' | 'fullwidth' | 'square';
  ```

* For naming props interface use `I` prefix in CamelCase followed by `component name` and suffixed with `Props`.

  ```
  // bad
  interface ICard {
    ...
  }

  // good
  interface ICardProps {
    ...
  }
  ```

* Prefer `type` before `enum` to define a name collection.

  ```
  // bad
  enum sizes {
    small = 'small',
    medium = 'medium',
    large = 'large',
  }

  // good
  type TButtonSizes = 'small' | 'medium' | 'large';
  ```

### **Declaration**

  * Use arrow functions to declare components.
  
      ```
      // bad
      function Listing({ hello }) {
        return <div>{hello}</div>;
      }
  
      // good
      const Listing = ({ hello }) => (
        <div>{hello}</div>
      );
      ```
  
  * Prefer default export over named export.
  [eslint: import/prefer-default-export](https://github.com/benmosher/eslint-plugin-import/blob/master/docs/rules/prefer-default-export.md)
    > :warning: **Exception**: Do not use `default export` in `storybook`. Use `named export` instead. `storybook` do not know how to interpret `default export`
  
    ```
    // bad
    export function foo() {}
    
    // good
    export default function foo() {}
    ```

  * Use camelCase when you export-default a function. Your filename should be identical to your function’s name.

    ```
    const Listing = () => {
      // ...
    };
  
    export default Listing;
    ```

### **Alignment**

* Follow these alignment styles for JSX syntax.
[eslint: react/jsx-closing-bracket-location](https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-closing-bracket-location.md),
[react/jsx-closing-tag-location](https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-closing-tag-location.md)

    ```
    // bad
    <Foo superLongParam="bar"
        anotherSuperLongParam="baz" />

    // good
    <Foo
      superLongParam="bar"
      anotherSuperLongParam="baz"
    />

    // if props fit in one line then keep it on the same line
    <Foo bar="bar" />

    // children get indented normally
    <Foo
      superLongParam="bar"
      anotherSuperLongParam="baz"
    >
      <Quux />
    </Foo>
    ```

### **Quotes**

* Always use double quotes (") for JSX attributes, but single quotes (') for all other JS.
[eslint: jsx-quotes](https://eslint.org/docs/rules/jsx-quotes)

  >Why? Regular HTML attributes also typically use double quotes instead of single, so JSX attributes mirror this convention.

  ```
  // bad
  <Foo bar='bar' />

  // good
  <Foo bar="bar" />

  // bad
  <Foo style= />

  // good
  <Foo style= />
  ```

### **Spacing**

* Always include a single space in your self-closing tag.
[eslint: no-multi-spaces](https://eslint.org/docs/rules/no-multi-spaces),
[react/jsx-tag-spacing](https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-tag-spacing.md)

  ```
  // bad
  <Foo/>

  // very bad
  <Foo                 />

  // bad
  <Foo
  />

  // good
  <Foo />
  ```

* Do not pad JSX curly braces with spaces.
[eslint: react/jsx-curly-spacing](https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-curly-spacing.md)

  ```
  // bad
  <Foo bar={ baz } />

  // good
  <Foo bar={baz} />
  ```

### **Props**

* Always use camelCase for prop names.

  ```
  // bad
  <Foo
    UserName="hello"
    phone_number={12345678}
  />

  // good
  <Foo
    userName="hello"
    phoneNumber={12345678}
  />
  ```

* Omit the value of the prop when it is explicitly `true`.
[eslint: react/jsx-boolean-value](https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-boolean-value.md)

  ```
  // bad
  <Foo
    hidden={true}
  />

  // good
  <Foo
    hidden
  />

  // good
  <Foo hidden />
  ```
* Avoid using an array index as `key` prop, prefer a unique ID.

  > [Why?](https://medium.com/@robinpokorny/index-as-a-key-is-an-anti-pattern-e0349aece318)
  To demonstrate the potential danger [see this simple example](https://jsbin.com/wohima/edit?output)

    ```
    // bad
  {todos.map((todo, index) =>
    <Todo
      {...todo}
      key={index}
    />
  )}

  // good
  {todos.map(todo => (
    <Todo
      {...todo}
      key={todo.id}
    />
  ))}
  ```

### **Refs**

* Always use ref callbacks.
[eslint: react/no-string-refs](https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/no-string-refs.md)

  ```
  // bad
  <Foo
    ref="myRef"
  />

  // good
  <Foo
    ref={(ref) => { this.myRef = ref; }}
  />
  ````

### **Parentheses**

* Wrap JSX tags in parentheses when they span more than one line.
[eslint: react/jsx-wrap-multilines](https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-wrap-multilines.md)

  ```
  // bad
  render() {
    return <MyComponent variant="long body" foo="bar">
            <MyChild />
          </MyComponent>;
  }

  // good
  render() {
    return (
      <MyComponent variant="long body" foo="bar">
        <MyChild />
      </MyComponent>
    );
  }

  // good, when single line
  render() {
    const body = <div>hello</div>;
    return <MyComponent>{body}</MyComponent>;
  }
  ```

### **Tags**

* Always self-close tags that have no children.
[eslint: react/self-closing-comp](https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/self-closing-comp.md)

  ```
  // bad
  <Foo variant="stuff"></Foo>

  // good
  <Foo variant="stuff" />
  ```

* If your component has multi-line properties, close its tag on a new line.
[eslint: react/jsx-closing-bracket-location](https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-closing-bracket-location.md)

  ```
  // bad
  <Foo
    bar="bar"
    baz="baz" />

  // good
  <Foo
    bar="bar"
    baz="baz"
  />
  ```

### **Conditionals**

*  Use `Inline condition` in JSX if there is only one variable to check.

    ```
    <div>
      {isLoggedIn && <LoginButton />
    </div>
    ```

* Use `Ternary condition` in JSX if the condition has an alternative.

  ```
  <div>
    {isLoggedIn ? <LogoutButton /> : <LoginButton />}
  </div>
  ```

* Create a `Helper function` inside the component and use it
in `JSX` if we have to check more than one variable to determine whether to render a component or
not.
  > Why? The readability is
  strongly impacted if we use the inline condition in this cases

  ```
  const canShowSecretData = () => {
    const { dataIsReady, isAdmin, userHasPermissions } = props;
    return dataIsReady && (isAdmin || userHasPermissions)
  }

  <div>
    {canShowSecretData() && <SecretData />}
  </div>
  ```

### **Iterators**

* Use [map method](https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Objetos_globales/Array/map) to display lists of items in JSX.

  ```
  <ul>
    {users.map(user =><li>{user.name}</li>)}
  </ul>
  ```

* Don’t use iterators. Prefer JavaScript’s higher-order functions instead of loops like `for-in` or `for-of`.
[eslint: no-iterator](https://eslint.org/docs/rules/no-iterator.html) [no-restricted-syntax](https://eslint.org/docs/rules/no-restricted-syntax)

  > Why? This enforces our immutable rule. Dealing with pure functions that return values is easier to reason about than side effects.

  > Use map() / every() / filter() / find() / findIndex() / reduce() / some() / ... to iterate over arrays, and Object.keys() / Object.values() / Object.entries() to produce arrays so you can iterate over objects.

  ```
  const numbers = [1, 2, 3, 4, 5];

  // bad
  let sum = 0;
  for (let num of numbers) {
    sum += num;
  }

  // good (keeping it functional)
  const sum = numbers.reduce((total, num) => total + num, 0);

  // bad
  const increasedByOne = [];
  for (let i = 0; i < numbers.length; i++) {
    increasedByOne.push(numbers[i] + 1);
  }

  // good (keeping it functional)
  const increasedByOne = numbers.map(num => num + 1);
  ```

### **Methods**

* Always define methods with arrow functions.
  ```
  // bad
  function foo() {
    ...
  }

  // good
  const foo = () => {
    ...
  };
  ```

* Use camelCase when naming functions and instances. eslint: camelcase:

  ```
  // bad
  const ThisIsMyFunction = () => {};
  const this_Is_My_Function = () => {};

  // good
  const thisIsMyFunction = () => {};
  ```
  
* Use `handle<Subject><Event>` convention when naming event handlers functions.

  ```
  // bad
  <Foo
    onNameChange={nameChange}
    onFormReset={reset}
    onClick={submit}
  />

  // good
  <Foo
    onNameChange={handleNameChange}
    onFormReset={handleFormReset}
    onClick={handleClick}
  />

  ```

* Use arrow functions to close over local variables.

  ```
  return (
    <ul>
      {props.items.map((item, index) => (
        <Item
          key={item.key}
          onClick={() => doSomethingWith(item.name, index)}
        />
      ))}
    </ul>
  );
  ```

* Be sure to return a value in your render methods.
[eslint: react/require-render-return](https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/require-render-return.md)

  ```
  // bad
  render() {
    (<div />);
  }

  // good
  render() {
    return (<div />);
  }
  ```

### **Hooks**

* [Split state into multiple state variables based on which values tend to change together.](https://reactjs.org/docs/hooks-faq.html#should-i-use-one-or-many-state-variables)

  ```
  // bad
  const [state, setState] = useState({ left: 0, top: 0, width: 100, height: 100 });

  useEffect(() => {
    const handleWindowMouseMove = (e) => {
        setState(state => ({ ...state, left: e.pageX, top: e.pageY }));
    }
    ...

  // good
  const [position, setPosition] = useState({ left: 0, top: 0 });
  const [size, setSize] = useState({ width: 100, height: 100 });

  useEffect(() => {
    const handleWindowMouseMove = (e) => {
      setPosition({ left: e.pageX, top: e.pageY });
    }
    ...
  ```

* [Only call Hooks at the top level. Don’t call Hooks inside loops, conditions, or nested functions.](https://es.reactjs.org/docs/hooks-rules.html#explanation)

  ```
  // bad
  const Form = () => {
    const [name, setName] = useState('Mary');

    if (name !== '') {
      useEffect(() => {
        const persistForm = () => localStorage.setItem('formData', name);
        persistForm();
      });
    }
    ...

  // good
  const Form = () => {
    const [name, setName] = useState('Mary');

    useEffect(() => {
      const persistForm = () => localStorage.setItem('formData', name);
      if (name !== '') {
        persistForm();
      }
    });
  ```

* [Only call Hooks from React function components. Don’t call Hooks from regular JavaScript functions.]((https://es.reactjs.org/docs/hooks-rules.html#explanation))

  ```
  // bad
  const handleInputChange = (value, setState) => {
    setState(value);
  }

  const Form = () => {
    const [name, setName] = useState('');

    return {
      <label for="name">First name:</label>
      <input
        type="text"
        value={name}
        onChange={(e) => handleInputChange(e.target.value, setName)} />
    }
  }

  // good
  const Form = () => {
    const [name, setName] = useState('');

    const handleInputChange = (event) => {
      setName(event.target.value);
    }

    return {
      <label for="name">First name:</label>
      <input
        type="text"
        value={name}
        onChange={handleInputChange} />
    }
  }
  ```
* Custom hooks should always start with `use`.
  >Why? Without it, eslint wouldn’t be able to automatically check for violations of rules of Hooks because it couldn’t tell if a certain function contains calls to Hooks inside of it.

  ```
  // bad
  const getFriendStatus = (friendID) => {
    const [isOnline, setIsOnline] = useState(null);
    ...
    return isOnline;
  }

  // good
  const useFriendStatus = (friendID) => {
    const [isOnline, setIsOnline] = useState(null);
    ...
    return isOnline;
  }
  ```

* Custom hooks should be placed in `hooks` directory inside `commons` directory.

  ```bash
  src
  └─ commons/
     └─ hooks/
  ```

### **React State with Arrays**

* Use array spreads `...` to copy arrays or add elements to an array.

  ```
  // bad
  const [commentList, setCommentList] = useState<string[]>([]);

  const newCommentList = commentList;
  newCommentList.push('New element');
  setCommentList(newCommentList);

  // good
  const [commentList, setCommentList] = useState<string[]>([]);

  setCommentList([...commentList, 'New element']);
  ```

### **React State with Objects**

* Prefer the object spread operator over `Object.assign` to shallow-copy objects. Use the object rest operator to get a new object with certain properties omitted.

  ```
  // very bad
  const [original, setOriginal] = useState({ a: 1, b: 2 });
  const copy = Object.assign(original, { c: 3 }); // this mutates `original`
  setOriginal(copy);

  // bad
  const [original, setOriginal] = useState({ a: 1, b: 2 });
  const copy = Object.assign({}, original, { c: 3 }); // copy => { a: 1, b: 2, c: 3 }
  setOriginal(copy);

  // good
  const [original, setOriginal] = useState({ a: 1, b: 2 });
  const copy = { ...original, c: 3 }; // copy => { a: 1, b: 2, c: 3 }
  setOriginal(copy);

  const { a, ...noA } = copy; // noA => { b: 2, c: 3 }
  ```

### **Typing**

* Use `React.FC` to declare Functional Components.
If the component receives props, add the interface of the props to the component declaration.

  ```
  // bad - without props
  const Card = () => {
    ...
  };

  // bad - with props
  const Card: React.FC = () => {
    ...
  };

  // good - without props
  const Card: React.FC = () => {
    ...
  };

  // good - with props
  const Card: React.FC<ICardProps> = (props: ICardProps) => {
    ...
  };
  ```

* `useState hooks` must be explicitly typed.

    ```
    // bad
    const [comment, setComment] = useState('');
    const [count, setCount] = useState(0);
    const [commentList, setCommentList] = useState([]);
    const [user, setUser] = useState({
      name: '',
      lastName: '',
    });

    // good
    const [comment, useComment] = useState<string>('');
    const [count, useCount] = useState<number>(0);
    const [commentList, commentList] = useState<string[]>([]);
    const const [user, setUser] = useState<IUser>({
      name: '',
      lastName: '',
    });
    ```
* `event handlers` must be typed.
  > See the [React typings](https://github.com/DefinitelyTyped/DefinitelyTyped/blob/master/types/react/index.d.ts) for all possible event types.

  ```
  // bad
  const onFooChange = (e: any) => {
    ...
  }

  // good
  const onFooChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    ...
  }
  ```

### **Ordering**

* Ordering for `React.FC`:

  1. component definition.
  1. primitive hooks // useTranslation, useStyles, useHistory, useDispatch, etc
  1. declare states with useState hook.
  1. custom hooks.
  1. clickHandlers or eventHandlers.
  1. getter methods for render
  1. useEffect hooks.
  1. return (render)

  ```
  // component definition

  const Form: React.FC<IFormProps> = (props: IFormProps) => {

    // useState hook
    const [width, setWidth] = useState(window.innerWidth); // State variable

    // custom hooks
    const useNameFormInput = createUseFormInput();
    const useSurnameFormInput = createUseFormInput();

    // event handlers
    const handleNameChange = (e) => {
      setName(e.target.value);
    };

    const handleSurnameChange = (e) => {
      setSurname(e.target.value);
    };

    // getter method for render
    const getUserFormFields = () => {
      return (
        <>
          <input value={name} onChange={handleNameChange} />
          <input value={surname} onChange={handleSurnameChange} />
        </>
      );
    }

    // useEffect hook
    useEffect(() => {
      const handleResize = () => setWidth(window.innerWidth);
      window.addEventListener('resize', handleResize);
      return () => window.removeEventListener('resize', handleResize);
    });

    // render
    return (
      <>
        {getUserFormFields()}
        <p>Hello, {name} {surname}</p>
        <p>Window width: {width}</p>
      </>
    );
  }

  export default Form;
  ```

   #### [Back to README](./README.md)
