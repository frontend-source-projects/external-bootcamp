# Material-UI - React

## Introduction

Material-UI is the world's most popular library for the development of UI with React JS. It implements Google's Material Design guidelines.

## Installation

### Material-ui

```node
// with npm
npm install @material-ui/core

// with yarn
yarn add @material-ui/core
```

#### SVG Icons

In order to use prebuilt SVG Material icons, first install the `@material-ui/icons` package:

```node
// with npm
npm install @material-ui/icons

// with yarn
yarn add @material-ui/icons
```

## Usage

Get started with React and Material-UI in no time.

Material-UI components work in isolation. They are self-supporting, and will only inject the styles they need to display. They don't rely on any global style-sheets such as normalize.css.

You can use any of the components as demonstrated in the documentation

### Quick start

Here's a quick example to get you started, it's literally all you need:

```js
import * as React from 'react';
import Button from '@material-ui/core/Button';

export default function App() {
  return (
    <Button variant="contained" color="primary">
      Hello World!
    </Button>
  );
}
```

## Styles

### Folder structure

For a good organization of styles we use the following folder structure:

Inside the themes folder we will create a folder for each different theme we have.
In this case we only create the default theme.
Everything is explained below.

```
[-] src/
 |- [-] styles/
 |   |- [-] themes/
 |   |   |- [-] default/
 |   |   |   |- [+] base/
 |   |   |   |- [+] overrides/
 |   |   |   |- index.tsx
```

#### `base`

The `base` folder contains a file where the entire color palette is organized globally.

```
[-] src/
 |- [-] styles/
 |   |- [-] themes/
 |   |   |- [-] default/
 |   |   |   |- [-] base/
 |   |   |   |   |- color-palette.ts
 |   |   |   |- [+] overrides/
 |   |   |   |- index.tsx
```

#### `overrides`

The `overrides` folder you configure the fonts in case you have external fonts, the global styles, and the global styles of the material components ui (MuiComponent.ts) for example MuiButton.ts.

```
[-] src/
 |- [-] styles/
 |   |- [-] themes/
 |   |   |- [-] default/
 |   |   |   |- [+] base/
 |   |   |   |- [-] overrides/
 |   |   |   |   |- fonts.ts
 |   |   |   |   |- global.ts
 |   |   |   |   |- MuiComponent.ts
 |   |   |   |- index.tsx
```

### Theming

Customize Material-UI with your theme. You can change the colors, the typography and much more.

Creating a sample theme:

#### `default/index.ts`

```js
export const defaultTheme: Theme = createMuiTheme({
  palette: {
    primary: {
      main: ColorPalette.seatPrimary,
    },
    secondary: {
      main: ColorPalette.seatSecondary,
    },
    error: {
      main: ColorPalette.red,
    },
    background: {
      default: ColorPalette.seatBackground,
    },
  },
  typography: {
    fontFamily: 'seat, sans-serif',
  },
  overrides: {
    MuiCssBaseline: {
      '@global': {
        ...globalOverrides,
      },
    },
    MuiButton,
    MuiTab,
  },
});
```

In this example we are creating the theme "defaultTheme" and adding some global styles such as

- Color Palette
- Typography
- Overrides
  - Within overrides we apply the global styles for the application.
  - Besides, we apply the global styles of each component (Button, Tab)

#### Theme provider

Since the frontend line wants to reuse components we have to create a common theme to be used by all developers.

`ThemeProvider` relies on the context feature of React to pass the theme down to the components, so you need to make sure that `ThemeProvider` is a parent of the components you are trying to customize.

In this case we will use the theme we created earlier.

#### `src/index.ts` <a id="index.ts"></a>

```js
ReactDOM.render(
  <Provider store={store}>
    <ThemeProvider theme={defaultTheme}>
      <CssBaseline />
      <Routes history={history} />
    </ThemeProvider>
  </Provider>,
  document.getElementById('app')
);
```

### CssBaseline

Material-UI provides an optional CssBaseline component. It fixes some inconsistencies across browsers and devices while providing slightly more opinionated resets to common HTML elements.

Basically, it resets your CSS to a consistent baseline. That way, you can restyle your HTML doc so that you know you can expect all of the elements to look the same across all browsers.

#### Global reset

You might be familiar with normalize.css, a collection of HTML element and attribute style-normalizations.

```js
import React from 'react';
import { CssBaseline } from '@material-ui/core';

export default function MyApp() {
  return (
    <React.Fragment>
      <CssBaseline />
      {/* The rest of your application */}
    </React.Fragment>
  );
}
```

In the previous section it is already implemented as an example. [src/index.ts](#index.ts)

### Responsive UI

Material Design layouts encourage consistency across platforms, environments, and screen sizes by using uniform elements and spacing.

Responsive layouts in Material Design adapt to any possible screen size. We provide the following helpers to make the UI responsive:

- [Grid](https://material-ui.com/components/grid/): The Material Design responsive layout grid adapts to screen size and orientation, ensuring consistency across layouts.
- [Container](https://material-ui.com/components/container/): The container centers your content horizontally. It's the most basic layout element.
- [Breakpoints](https://material-ui.com/customization/breakpoints/): API that enables the use of breakpoints in a wide variety of contexts.
- [useMediaQuery](https://material-ui.com/components/use-media-query/): This is a CSS media query hook for React. It listens for matches to a CSS media query.
- [Hidden](https://material-ui.com/components/hidden/): Quickly and responsively toggle the visibility value of components and more with our hidden utilities.

### Customization component

#### Basic

There are 3 possible APIs you can use to generate and apply styles, however they all share the same underlying logic.

- Hook API
- Styled components API
- Higher-order component API

By convention we will use Hook API. In each component we will apply its own styles.

Example of a component:

- `App.tsx`: Here we will create the component and import the styles from the other file.
- `App.styles.ts`: Here we will create our own styles so that we can have them uncoupled.

##### `App.tsx`

```js
import * as React from 'react';
import Button from '@material-ui/core/Button';
import appStyles from './App.styles';

export default function App() {
  const classes = appStyles({});

  return <Button className={classes.root}>Hello Word!</Button>;
}
```

##### `App.styles.ts`

```js
import { makeStyles } from '@material-ui/core/styles';

const appStyles = makeStyles({
  root: {
    background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
    border: 0,
    borderRadius: 3,
    boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
    color: 'white',
    height: 48,
    padding: '0 30px',
  },
});

export default appStyles;
```

### Styles with SCSS

#### Controlling priority

JSS injects its styles at the bottom of the <head>. If you don't want to mark style attributes with !important, you need to change the CSS injection order.

##### `src/index.ts`

```js
import { StylesProvider } from '@material-ui/core/styles';

ReactDOM.render(
  <Provider store={store}>
    <StylesProvider injectFirst>
      <CssBaseline />
      <AppLayout history={history} />
    </StylesProvider>
  </Provider>,
  document.getElementById('app')
);
```

#### Customization Component

To customize our component we will do it in the following way. An important point is that the classes are global since they are not yet prepared with CSS Modules.

##### `ButtonCustom.tsx`

```tsx
import React from 'react';
import Button from '@material-ui/core/Button';

export default function PlainCssButton() {
  return (
    <div>
      <Button>Default</Button>
      <Button className="buttonCustom">Customized</Button>
    </div>
  );
}
```

##### `ButtonCustom.scss`

```scss
.buttonCustom {
  background-color: #6772e5;
  color: #fff;
  box-shadow: 0 4px 6px rgba(50, 50, 93, 0.11), 0 1px 3px rgba(0, 0, 0, 0.08);
  padding: 7px 14px;

  &:hover {
    background-color: #5469d4;
  }
}
```

We can also modify the classes that belong to that component, You can target the class names generated by Material-UI.

```scss
.MuiButton {
  &-root {
    background-color: #6772e5;
    box-shadow: 0 4px 6px rgba(50, 50, 93, 0.11), 0 1px 3px rgba(0, 0, 0, 0.08);
    padding: 7px 14px;
    &:hover {
      background-color: #5469d4;
    }
  }

  &-label {
    color: #fff;
  }
}
```

## Supported Platforms

### Browser

Material-UI supports the latest, stable releases of all major browsers and platforms. It also supports Internet Explorer 11. You don't need to provide any JavaScript polyfill as it manages unsupported browser features internally and in isolation.

| IE  | Edge  | Firefox | Chrome | Safari | Googlebot          |
| --- | ----- | ------- | ------ | ------ | ------------------ |
| 11  | >= 14 | >= 52   | >= 49  | >= 10  | :white_check_mark: |
