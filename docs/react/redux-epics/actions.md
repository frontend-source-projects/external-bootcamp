Actions
==============================

## How to create actions?

An action MUST

* be a plain JavaScript object.
* have a `type` property.

An action MAY

* have a `payload` property.

### Example

A basic Action:

```js
{
  type: 'ADD_TODO',
}
```

An example with optional parameters:

```js
{
  type: 'ADD_TODO',
  payload: { randomProperty:'random' },
}
```
### `type`

The `type` property must be a string constant.  

```ts

export const ADD_TODO = 'ADD_TODO';

const addTodoAction = { type: ADD_TODO }

// addTodo is an example of action creator (a function that returns an action). We should try to consume the actions through actions creators
export const addTodo = () => addTodoAction;

```  

## Async Actions  
Usually, for any API request you'll want to dispatch at least three different kinds of actions:

* **An action informing the reducers that the request began.**

The reducers may handle this action by toggling an isLoading flag in the state. This way the UI knows it's time to show a spinner.

* **An action informing the reducers that the request finished successfully.**

The reducers may handle this action by merging the new data into the state they manage and resetting isLoading. The UI would hide the spinner, and display the fetched data.

* **An action informing the reducers that the request failed.**

The reducers may handle this action by resetting isLoading. Additionally, some reducers may want to store the error message so the UI can display it.

In that casuistic, `our adopted typing convention for the actions will be:`  
( This usually will be placed inside ./store/feature-group/feature-group.types.ts )
```ts

export const TODO_LIST_REQUEST = 'TODO_LIST_REQUEST';
export const TODO_LIST_SUCCESS = 'TODO_LIST_SUCCESS';
export const TODO_LIST_FAILURE = 'TODO_LIST_FAILURE';

export interface TodosRequestAction {
    type: typeof TODO_LIST_REQUEST;
    payload?: any;
}

export interface TodosSuccessAction {
    type: typeof TODO_LIST_SUCCESS;
    success: T;
}

export interface TodosFailureAction {
    type: typeof TODO_LIST_FAILURE ;
    error: string;
}

export type TodoActionTypes = TodosRequestAction | TodosSuccessAction | TodosFailureAction;

export interface TodosActions {
    todoRequest: (payload?:any) => TodoActionTypes;
    todoSuccess: (success: T) => TodoActionTypes;
    todoFailure: (error: string) => TodoActionTypes;
}

```
And our ``implementation of the actions creators will be:``  
( This usually will be placed inside ./store/feature-group/feature-group.actions.ts )
```ts
import { TODO_LIST_REQUEST, TODO_LIST_SUCCES, TODO_LIST_FAILURE, TodosActions } from '../../store/idp-roles/idp-roles.types';

const todoActionsCreators: TodoActions = {
    todoRequest: (payload?) => ({
        type: TODO_LIST_REQUEST,
        payload,
    }),
    todoSuccess: (success) => ({
        type: TODO_LIST_SUCCES,
        success,
    }),
    todoFailure: (error) => ({
        type: TODO_LIST_FAILURE,
        error,
    }),
};

// an object that have action creators as properties
export default todoActionsCreators;
```
``The place where the logic is placed is under open discussion!!!``

#### [Back to README](./README.md)
