Reducers
=========================  

## How to create reducers in this project  

Reducers specify how the application's state changes in response to actions sent to the store.  
The reducer is a pure function that takes the previous state and an action, and returns the next state.
```js
(previousState, action) => nextState
```
Things you should never do inside a reducer:

- Mutate its arguments;
- Perform side effects like API calls and routing transitions;
- Call non-pure functions, e.g. Date.now() or Math.random().

## Designing the reducer state shape

Since we are using combineReducers( ), first of all, we should decide the shape of the data joined to the app tree state by this new reducer. 

```ts
export interface TodoStore {
  todos: Todo[]; // Todo type must be modeled inside models folder
  isLoading: boolean;
  errorMessage: string;
}

const TODO_INITIAL_STATE: TodoStore = {
  todos: [],
  isLoading: false,
  errorMessage: '',
};

```
Once we have the state, we are able to continue defining the reduce for all todo actions

```ts

import { TODO_LIST_REQUEST, TODO_LIST_SUCCESS, TODO_LIST_FAILURE, TodoActionTypes} from './wherever-the-actions-are';

export default function todoReducer(previousState: TodoStore = TODO_INITIAL_STATE, action: TodoActionTypes ) {
  switch (action.type) {
    case TODO_LIST_REQUEST:
      return {
        ...previousState,
        isLoading: true,
      }
    case TODO_LIST_SUCCESS:
      return {
        ...previousState,
        todos: action.success,
        isLoading: false,
        errorMessage: '',
      }
    case TODO_LIST_FAILURE:
      return {
        ...previousState,
        todos: [],
        isLoading: false,
        errorMessage: action.error,
      }
    default:
      return previousState;
  }
}

```
Add the reducer in combineReducers( ) call

```ts
import todoReducer from 'wherever-the-reducer-is-defined'

export const rootReducer = combineReducers({
  todoReducer
});
```
Since this combination, our AppState will adquire a new property called ``todoReducer``. That property will be equal to the currentState of ``TodoStore``.

#### [Back to README](./README.md)
