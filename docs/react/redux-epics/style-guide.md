Style Guide for React - Redux - Epics
==================================

## What is the current naming for the folder structure of the data flow?
The naming convention used is ``kebab-case``.

## What is the current folder structure for the data flow?

## ``Core``
All the documents related with the data/state will be placed under the folder called `core`.  

    core  
    ├── epics  
    ├── models  
    ├── service-api  
    └── store  

### ``Epics``
Within Epics folder is where we will ``define all new Epics``. Any epic within a new folder that will contain the ``name-of-the-epic-feature.epic.ts`` and the ``name-of-the-epic-feature.epic.spec.ts``.  
Also there will be an ``index.ts`` in the root, where we will ``combine all created epics``( using the combineEpics( )) that we want to be added inside the middleware during the creation of the store.
   
    ├── epics                   
    │   ├── name-of-the-epic-feature  
    |   |   ├── name-of-the-epic-feature.epic.ts  
    |   |   └── name-of-the-epic-feature.epic.spec.ts
    |   └── index.ts
    └── ...

### ``Models``
Within models folder is where we will ``define all data models``..    
   
    ├── models                   
    │   ├── name-of-the-model.ts  
    |   ├── name-of-the-model-2.ts  
    |   └── ...
    └── ...

### ``Services``
Within services-api folder is where we will group all new ``services related with the dealing of the data``. The convention is to group the services by feature groups.  
The idea of the services is to abstract logic from the epic to improve legibility, testability and reutilization.  
We must remember that we will not be able to use any redux hook within them.
   
    ├── services-api                   
    │   ├── data-group-name
    |   |   ├── service-1.ts  
    |   |   └── service-2.ts
    |   └── data-group-name-2
    |       └── ...
    └── ...

### ``Store``
Within store folder is where we will define all code related with Redux.  
The code will be grouped by feature and the convention is that always will be actions, reducers and types per group.   
``.actions.ts`` -> Is where we will export all action creators related with the feature group. [+info](./actions.md)   
``.reducer.ts`` -> Is where we will define the reducers for the actions defined in actions.md [+info](./reducers.md)    
``.types.ts`` -> Is where we will define and export all interfaces related with the feature group that will be used in the actions, reducers and epics.    
Also there will be an ``index.ts`` in the root, where we will combine the reducers, define the middleware and create the store with them.

    ├── store
    │   ├── name-of-the-feature-group
    |   |   ├── name-of-the-feature-group.actions.ts
    |   |   ├── name-of-the-feature-group.reducer.ts
    |   |   └── name-of-the-feature-group.types.ts
    |   ├── name-of-the-feature-group-2
    |   |   └── ...
    |   |
    |   ├── ...
    |   └── index.ts
    └── ...

Style Guide for Redux
==================================
We strongly recommend the read of [Redux Style Guide](https://redux.js.org/style-guide/style-guide).
Some of the recommendations are followed by this seed. There you will found more arguments to understand why.
``Would be amazing to share opinions about the recomendations within this guide!!!``


#### [Back to README](./README.md)
