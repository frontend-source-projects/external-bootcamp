How to work with Redux-Epics data flow: 
==============================

First of all we recommend to read the architecture [style guide](./style-guide.md) to know what is the followed folder structure and naming.

## Steps to develop new features:

### 1. With the feature in mind, detect what data is desired/required to be stored in the Redux store. [When to store it?](https://redux.js.org/faq/organizing-state#do-i-have-to-put-all-my-state-into-redux-should-i-ever-use-reacts-setstate).  
Also, is good to share your decisions with the team to enhance consistency.
### 2. Define what actions will be required for managing the state of this new data. [How to implement actions](./actions.md).
### 3. Define if there are some side effects, log, api call, .... If yes? We should implement some middleware listening for any action dispatch. [How to implement epics](./epics.md).
### 4. Define the reducers for all actions defined. [How to implement reducers](./reducers.md).

## New most useful tools that Redux 7.1+ offer us to work with:

- ``useDispatch( )`` [+info](https://react-redux.js.org/api/hooks#usedispatch)
- ``useSelector( )`` [+info](https://react-redux.js.org/api/hooks#useselector)
