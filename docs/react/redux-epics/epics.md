Redux-Observable
==========================

## How to create Epics in this project 
Since within any normal flow of redux is not recommended to perfom side effecs, the users of Redux started to use middlewares.  
In our case, an observables based middleware is used to allow us to execute side effects like API calls.  
That middleware uses functions called `Epics`, thouse will be listening for all dispached actions.  
Epics has roughly this type `signature`:

```ts
/**
 * @param action$ An observable of the action object
 * @param state$ A custom stateObservable. It will recieve as argument a stream of store states. With this, you can use state$.value to synchronously access the current state. For example, useful to obtain a session token added to the store.
 */
function (action$: Observable<Action>, state$: StateObservable<State>): Observable<Action>;

```
Once you are inside your Epic, use any Observable patterns you desire as long as any output from the final, returned stream, is an action. 
The stream actions you return will be immediately dispatched through the normal store.dispatch().   
Thats because, under the hood redux-observable effectively does  
```js 
epic(action$, state$).subscribe(store.dispatch)
```  
#### [- More info of state$ param usage](https://redux-observable.js.org/docs/basics/Epics.html)
## Example of an Epic of a basic API call to fetch data
```ts 
// first we need to import the actions that will be defined as a final response of the epic
import { todoActionsCreators } from 'wherever-the-actions-were-declared'

// also the action that will be used inside the ofType operator to filter the execution of the side effects
import { TODO_LIST_REQUEST } from './wherever-the-actions-are';

// import the function that returns the ajax observable from the service
import { getTodosApiCall } from 'wherever-the-service-is';

// other imports required by the epic
import { ofType } from 'redux-observable'; // operator that let us filter the epic execution by action
import { Observable, of } from 'rxjs';
import { switchMapTo, map, catchError } from 'rxjs/operators';

// finally we define the Epic that will execute getTodosApiCall if action.type is of TODO_LIST_REQUEST type
const todosEpic = (action$: Observable<Action>) => {
    return action$.pipe(
        ofType(TODO_LIST_REQUEST),
        switchMapTo(
            getTodosApiCall().pipe(
                map(todosListFromApiCall => {
                    // and return a success Observable<Action>
                    return todoActionsCreators.todoSuccess(todosListFromApiCall)
                }),
                catchError(errorFromApiCall => {
                    // or a failure Observable<Action>
                    return of(todoActionsCreators.todoFailure(errorFromApiCall))
                })
            )
        )
    )
}

export default todosEpic;

```  
As final step, we must add the new epic inside the combineEpics( ) function
```js 
const rootEpic = combineEpics(
  todosEpic,
);
```  
Since added to middleware by the combineEpics, all dispatched actions will be recieved as argument for the action$ param.
## Testing the basic Epic  

Since the type of the value returned by an Epic function is expected to equal to an Action. The test must lets us corroborate it.

#### [Back to README](./README.md)
