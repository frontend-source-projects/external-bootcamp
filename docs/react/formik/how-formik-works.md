# How Formik works

Formik keeps track of your form's state and then exposes it plus a few reusable
methods and event handlers (handleChange, handleBlur, and handleSubmit) to your
form via props. handleChange and handleBlur work exactly as expected--they use a
name or id attribute to figure out which field to update.

To reducing boilerplate Formik comes with a few extra components: `<Form />`,
`<Field />`, and `<ErrorMessage />`. They use React context to hook into the
parent `<Formik />` state/methods.

## Raw React forms vs Formik

Without Formik, a lot of boilerplate is needed to write our forms:

```jsx
({
  values,
  errors,
  touched,
  handleChange,
  handleBlur,
  handleSubmit,
  isSubmitting,
  // and other goodies...
}) => (
  <form onSubmit={handleSubmit}>
    <input
      type="email"
      name="email"
      onChange={handleChange}
      onBlur={handleBlur}
      value={values.email}
    />
    {errors.email && touched.email && errors.email}
    <input
      type="password"
      name="password"
      onChange={handleChange}
      onBlur={handleBlur}
      value={values.password}
    />
    {errors.password && touched.password && errors.password}
    <button type="submit" disabled={isSubmitting}>
      Submit
    </button>
  </form>
);
```

Using Formik components, we would have the following structure:

```jsx
({ isSubmitting }) => (
  <Form>
    <Field type="email" name="email" />
    <ErrorMessage name="email" component="div" />
    <Field type="password" name="password" />
    <ErrorMessage name="password" component="div" />
    <button type="submit" disabled={isSubmitting}>
      Submit
    </button>
  </Form>
);
```

## Simple login form

Simple login form with validations:

```jsx
import React from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";

const Basic = () => (
  <div>
    <h1>Any place in your app!</h1>
    <Formik
      initialValues={{ email: "", password: "" }}
      validate={(values) => {
        const errors = {};
        if (!values.email) {
          errors.email = "Required";
        } else if (
          !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
        ) {
          errors.email = "Invalid email address";
        }
        return errors;
      }}
      onSubmit={(values, { setSubmitting }) => {
        setTimeout(() => {
          alert(JSON.stringify(values, null, 2));
          setSubmitting(false);
        }, 400);
      }}
    >
      {({ isSubmitting }) => (
        <Form>
          <Field type="email" name="email" />
          <ErrorMessage name="email" component="div" />
          <Field type="password" name="password" />
          <ErrorMessage name="password" component="div" />
          <button type="submit" disabled={isSubmitting}>
            Submit
          </button>
        </Form>
      )}
    </Formik>
  </div>
);

export default Basic;
```

## Validations

As you can see above, validations are left to you. Feel free to write your own
validators or use a 3rd party library like [Yup](https://github.com/jquense/yup).

For more detailed information, follow the documentation on
[how validations work](./validations.md).
