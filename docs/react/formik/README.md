# Formik

Formik is a small group of React components and hooks for building forms in
React and React Native.

## Why use Formik?

Formik helps you to write the three most annoying parts of building a form:

- Getting values in and out of form state
- Validation and error messages
- Handling form submission

By colocating all of the above in one place, Formik keeps things organized
making testing, refactoring, and reasoning about your forms a breeze.

## Why not Redux-Form?

A form state it's ephemeral, so that tracking it on redux it's innecessary,
since this would cause redux calls on every single keystroke. So it would end
causing performance problems as our Redux app grows.

## Installation

```
npm install formik
```

## Articles

- [How Formik works](how-formik-works.md)
- [Validations](validations.md)

## References

- [Formik](https://jaredpalmer.com/formik/)
- [Yup](https://github.com/jquense/yup)
