# Formik validations

You can control when Formik runs validation by changing the values of
`<Formik validateOnChange>` and/or `<Formik validateOnBlur>` props depending on
your needs. By default, Formik will run validation methods as follows:

- After "change" events/methods (things that `updatevalues`) ->
  `handleChange, setFieldValue, setValues`
- After "blur" events/methods (things that update `touched`) ->
  `handleBlur, setTouched, setFieldTouched`
- Whenever submission is attempted ->
  `handleSubmit, submitForm`
- There are also imperative helper methods provided to you via Formik's
  render/injected props which you can use to imperatively call validation. ->
  `validateForm,validateField`

Formik supports synchronous and asynchronous form-level and field-level
validation. Furthermore, it comes with baked-in support for schema-based
form-level validation through Yup.

## Form-level validations

Have complete access to all of your form's values and props whenever the
function runs, so you can validate dependent fields at the same time.

There are 2 ways to do form-level validation, with validate, or using
`validationSchema`.

### Validate

`<Formik>` and `withFormik()` take prop/option called `validate` that accepts
either a synchronous or asynchronous function.

```js
const validate = (values, props /* only available when using withFormik */) => {
  const errors = {};

  if (!values.email) {
    errors.email = "Required";
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = "Invalid email address";
  }

  return errors;
};
```

### Validation Schema

Formik has a special config option / prop for `Yup` object schemas called
`validationSchema` which will automatically transform Yup's validation errors
into a pretty object whose keys match values and touched.

```
npm install yup
```

```jsx
import * as Yup from "yup";

const SignupSchema = Yup.object().shape({
  firstName: Yup.string()
    .min(2, "Too Short!")
    .max(50, "Too Long!")
    .required("Required"),
  lastName: Yup.string()
    .min(2, "Too Short!")
    .max(50, "Too Long!")
    .required("Required"),
  email: Yup.string().email("Invalid email").required("Required"),
});

export const ValidationSchemaExample = () => (
  <Formik
    initialValues={{
      firstName: "",
      lastName: "",
      email: "",
    }}
    validationSchema={SignupSchema}
  />
);
```

## Field-level validation

It's supported via the `<Field>`/`<FastField>` components' validate prop. This
function can be sync or async, and will only be executed on mounted fields. It
will run after any onChange and onBlur by default, that can be altered at the
top level `<Formik/>` component using the `validateOnChange` and
`validateOnBlur` props respectively.

```jsx
function validateEmail(value) {
  let error;
  if (!value) {
    error = "Required";
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)) {
    error = "Invalid email address";
  }
  return error;
}

function validateUsername(value) {
  let error;
  if (value === "admin") {
    error = "Nice try!";
  }
  return error;
}

export const FieldLevelValidationExample = () => (
  <Form>
    <Field name="email" validate={validateEmail} />
    {errors.email && touched.email && <div>{errors.email}</div>}
    <Field name="username" validate={validateUsername} />
    {errors.username && touched.username && <div>{errors.username}</div>}
  </Form>
);
```

## Manually Triggering Validation

You can manually trigger both form-level and field-level validation with Formik
using the `validateForm` and `validateField` methods respectively.

```jsx
<Form>
  <Field name="username" validate={validateUsername} />
  {errors.username && touched.username && <div>{errors.username}</div>}
  {/** Trigger field-level validation imperatively */}
  <button type="button" onClick={() => validateField('username')}>
    Check Username
  </button>
  {/** Trigger form-level validation imperatively */}
  <button type="button" onClick={() => validateForm().then(() => console.log('blah')))}>
    Validate All
  </button>
</Form>
```

## References

- [Formik Tutorial: Validation](https://jaredpalmer.com/formik/docs/tutorial#validation)
- [Formik Guides: Validation](https://jaredpalmer.com/formik/docs/guides/validation)
