
## Browser Engines

| Browsers | Render Engines | JScript Interpreter Engines | Versions |
| :----------- | :----: | :----: | :----: |
| Chrome | WebKit | V8 | <= 27 |
| Chrome | Blink | V8 | >= 80 |
| Edge | EdgeHTML | Chakra | <= 44 |
| Edge | Blink | Chakra | >= 79 |
| Firefox | Gecko | SpiderMonkey | >= 8.0 |
| Internet Explorer | Trident | - | >= 4.0 |
| Internet Explorer | Trident | Chakra | >= 9.0 | 
| Opera | Blink | V8 | >= 15 |
| Safari | WebKit | Nitro | -

## Browsers Compatibility

| Browsers | Versions |
| :----------- | :----: |
| Chrome | >= 72 |
| Chrome Android | >= 80 |
| Edge | >= 79 |
| Firefox | >= 75 |
| Firefox ESR | >= 68 |
| Firefox Android | >= 68 |
| Safari | >= 10.3 |
| Safari iOS | >= 10.3 |
| Opera | >= 58 |

## Known Issues

### Edge

- IE & Edge are reported to not support calc inside a 'flex'. This example does not work: flex: 1 1 calc(50% - 20px).
- Edge 18 and below has various problems supporting calc() within grid properties.

### Firefox
- Firefox <59 does not support calc() on color functions. Example: color: hsl(calc(60 * 2), 100%, 50%).
- Firefox <66 does not support width: calc() on table cells.
- Firefox <48 does not support calc() inside the line-height, stroke-width, stroke-dashoffset, and stroke-dasharray properties.


### Internet Explorer
- IE does not support calc() on color functions. Example: color: hsl(calc(60 * 2), 100%, 50%).
- IE & Edge are reported to not support calc inside a 'flex'. This example does not work: flex: 1 1 calc(50% - 20px);
- IE11 does not support transitioning values set with calc()
- IE11 is reported to not support calc() correctly in generated content
- IE10 crashes when a div with a property using calc() has a child with same property with inherit.
- IE10, IE11, and Edge < 14 don't support using calc() inside a transform.
- IE11 is reported to have trouble with calc() with nested expressions, e.g. width: calc((100% - 10px) / 3); (i.e. it rounds differently)
- IE 9 - 11 don't render box-shadow when calc() is used for any of the values
- IE 9 - 11 and Edge do not support width: calc() on table cells.
  
### Safari
- Safari & iOS Safari (both 6 and 7) does not support viewport units (vw, vh, etc) in calc().
- Safari does not yet support intrinsic and extrinsic sizing with grid properties such as grid-template-rows.