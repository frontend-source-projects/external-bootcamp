# How to test in jest without require()

## When/why to use dynamic import

As we always use the features of ES6, we will use dynamic import (`import()`) instead of `require()`.

We will use it when we want to dynamically mock different implementations of modules for different tests cases.

Using it, we avoid disabling eslint `global-require` rule in the project.

## Example

For showing how to use it we will test `my.service.ts` that depends on an api wrapper.

##### `utils/api.ts`

```js
export default class Api {
    private baseUrl: string;

    constructor(baseUrl: string) {
        this.baseUrl = baseUrl;
    }

    get(url: string, queryParams?: object) {
        return [`${this.baseUrl}/${url}`, queryParams, 'fake', 'data'];
    }
    ...
}
```

##### `services/my.service.ts`

```js
import Api from "../utils/api";

const api = new Api("base-url");

export const getSomething = (queryParams?: object) =>
  api.get("something-url", queryParams);

export const getAnotherThing = (queryParams?: object) =>
  api.get("another-thing-url", queryParams);
```

##### `services/my.service.spec.ts`

```js
describe("my service", () => {
  beforeEach(() => jest.resetModules());

  describe("getSomething", () => {
    it("should call api with url and params", async () => {
      const queryParams = { queryParam1: "abc" };
      const expectedResponse = ["fake", "response"];

      const get = jest.fn().mockReturnValue(expectedResponse);

      jest.doMock("../utils/api", () =>
        jest.fn().mockImplementation(() => ({
          get,
        }))
      );

      // bad
      const { getSomething } = require("./my.service");

      // good
      const { getSomething } = await import("./my.service");

      const response = getSomething(queryParams);

      expect(get).toHaveBeenCalledWith("something-url", queryParams);
      expect(response).toEqual(expectedResponse);
    });
  });

  describe("getAnotherThing", () => {
    it("should call api with url and params", async () => {
      const queryParams = { queryParam1: "def" };
      const expectedResponse = ["another", "response"];

      const get = jest.fn().mockReturnValue(expectedResponse);

      jest.doMock("../utils/api", () =>
        jest.fn().mockImplementation(() => ({
          get,
        }))
      );

      // bad
      const { getAnotherThing } = require("./my.service");

      // good
      const { getAnotherThing } = await import("./my.service");

      const response = getAnotherThing(queryParams);

      expect(get).toHaveBeenCalledWith("another-thing-url", queryParams);
      expect(response).toEqual(expectedResponse);
    });
  });
});
```
