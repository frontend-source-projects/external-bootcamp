# Unit test - Guide to what to test

## What do we test?

### HTML/CSS out of reach

There are tools to test UI but we discard them because we consider them to be part of integration tests and for the time being we prefer to focus on unit tests

### We focus on testing business rules

Although we want to cover both services and components with unit tests, we will focus on testing services initially as we prefer to test business rules rather than bindings with the interface

### We recommend decoupling business rules from Components

In order to have business rules with a high percentage of coverage we should try to decouple those rules from components in isolated functions in services

### Components out of scope

Let's try to just test functionality in services. This means:

- React: do not test the files `.tsx`
- Angular: do not test the files `.component.ts`
- All functionality will be in "services" `(.ts)`

### TDD out of reach

We want to end up doing TDD but we think we are at a very early stage and prefer to consolidate testing knowledge first so for the moment we will implement before testing

## Testing Styleguide

### BDD syntax

We chose BDD over TDD for its better readability

### File structure

- Extension `.spec.ts` (from BDD spectations)
- One test file to every service file
- The test must accompany the service (in the same directory)
- The stubs (data simulation) go outside the test on the same level `(.stub.ts)`
- The mocks (API simulation) go to the same level as the service they are mocking `(.mock.ts)`
- The stubs simulate inputs so we mainly want to condition them to each test but the mocks simulate dependencies so we mainly want to reuse them between the different tests

### Test structure

- One root `describe` per file with the filename as the content
- Inside, a `describe` per method or function with the function name as content
- Inside, one `it` per test
- Within, as many `expect` as necessary to ensure that the test is met with various test cases