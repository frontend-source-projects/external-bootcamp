# Unit testing

## What is a unit test?

A unit test is a piece of code that tests a single unit of production code. It
does so by setting up one or a few more objects in a known state, exercising
them (e.g., calling a method), and then inspecting the result, comparing it to
the expected outcome.

Unit tests should test software components in isolation (fully isolated from
dependencies).

## Concepts

### Test case

It is the individual unit of testing:

- Checks for a specific response to a particular set of inputs
- Answers the question: What am I going to test?
- Can have one or more assertions

### Test suite

It is a collection of test cases that are intended to be used to test a software
program to show that it has some specified set of behaviors. The following group
of test cases could be a real test suite:

- Test case 1: login
- Test case 2: dashboard
- Test case 3: logout

### Code coverage

It shows how much code we are really testing meaning which lines of code were or
were not being executed by the tests.

It is recommended to have an 80% of code covered by unit tests.

### Assertions

> Assert = affirm, make sure

An assertion is a predicate that states the programmer’s intended state of a
system. When an assertion fails, the test is aborted and the failure reason is
shown.

```js
const result = sum(3, 4);
const expected = 7;
expect(result).toBe(expected);
```

## Testing environment

### React

- Jest
- Enzyme

### Angular

- Jasmine
- Karma

## Best practices

### What should be tested?

We don't test components, we test functionality.

Cause we just test functionallity, it is important to move all the logic within
the component to services and keep the components as simpler as possible.

If a service has another dependant services, mock them for treating it as a
unit.

### How should be a unit test?

- **It needs to be fast**

  Faster tests mean faster feedback every time you edit your code. This will
  inevitably make the entire development process faster, while allowing you to
  run tests more frequently.

- **Altering the implementation detail should not break the code**

  In other words, “write the test with the interface in mind” or “do not write
  implementation dependent tests”. To look at it from another angle, this also
  applies to chunking the test units into too many small chunks. If the test
  cracks even at the slightest refactoring, not only does it deprive the test of
  its credibility, but also demands more time and resources to fix the tests.

- **It should be able to detect bugs**

  To put it differently, “tests that validate buggy codes should fail.” If the
  expectations are not specified in detail, or if the program does not test
  through every imaginable scenario, some bugs could go unnoticed. Also,
  excessive usage of mock objects can make it difficult to detect errors that
  could occur during the connection stages even the dependent objects are
  edited. Therefore, test specs must be comprehensive, and refrain from using
  mock objects as much as possible.

- **It should be consistent**

  If the test that worked perfectly yesterday suddenly doesn’t work today, or if
  the test that had no problem with certain devices but doesn’t run on other
  devices, you’d probably lose faith in the test. Good tests should minimize the
  external and environmental effects on the results, as to produce the same
  results no matter the given conditions. The environmental aspects include
  time, device OS, and network status, and good tests should be designed so that
  such elements can be directly manipulated using mock objects or external
  tools.

- **The intent of the test should be clear**

  By now, I think everyone acknowledges that code readability is important. One
  of the defining characteristics of good codes is that “people” can easily read
  and understand the code, not “machines.” Test codes should be held to the same
  standards; anyone should be able to look at the test code and tell you the
  purpose of the test. Illegible codes demand more resources when the code
  eventually has to be edited or removed. If the test requires repetitious and
  lengthy codes need to be constantly called or if validation codes are
  unnecessarily verbose, it is better to simply create a function or use
  assertion statements to handle such tasks.

## Resources

### Testing basics

- [New To Front End Testing?](https://dev.to/noriste/new-to-front-end-testing-start-from-the-top-of-the-pyramid-36kj)
- [Javascript Unit Testing for Beginners](https://designmodo.com/test-javascript-unit/)
- [UI Testing Best Practices](https://github.com/NoriSte/ui-testing-best-practices)
- [JavaScript and Node Testing Best Practices](https://github.com/goldbergyoni/javascript-testing-best-practices)
- [5 Questions Every Unit Test Must Answer](https://medium.com/javascript-scene/what-every-unit-test-needs-f6cd34d9836d)
- [Mocking is a code smell](https://medium.com/javascript-scene/mocking-is-a-code-smell-944a70c90a6a)
- [Rethinking Unit Test Assertions](https://medium.com/javascript-scene/rethinking-unit-test-assertions-55f59358253f)
- [An Overview of JavaScript Testing in 2019](https://medium.com/welldone-software/an-overview-of-javascript-testing-in-2019-264e19514d0a)
- [Pragmatic Front End Testing Strategies](https://medium.com/@toastui/pragmatic-front-end-testing-strategies-1-4a969ab09453)

### Testing observables with marble testing

- [Marble testing Observable Introduction](https://medium.com/@bencabanes/marble-testing-observable-introduction-1f5ad39231c)
- [Marble testing with RxJS testing utils](https://medium.com/@kevinkreuzer/marble-testing-with-rxjs-testing-utils-3ae36ac3346a)
- [Testing asynchronous RxJs operators](https://medium.com/angular-in-depth/testing-asynchronous-rxjs-operators-5495784f249e)
- [How to test Observables](https://medium.com/angular-in-depth/how-to-test-observables-a00038c7faad)
