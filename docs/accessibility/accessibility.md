# **Digital Accessibility**

# Index
- [What is it and Why it matters](#what-it-is-and-why-it-matters)
- [Terms glossary](#terms-glossary)
- [Types of disabilities](#types-of-disabilities)
- [Compliance Level](#compliance-level)
- [Coding for visual disabilities](#coding-for-visual-disabilities)
- [Accessible components examples](#accessible-components-examples)
- [Checklist for AA Compliance](#checklist-for-AA-Compliance)
- [Testing Accessibility](#testing-accessibility)


# What is it and Why it matters

> Accessibility is: freedom from discrimination

Digital accessibility is “the inclusive practice of removing barriers that prevent interaction with, or acces to websites/native apps, by people with disabilities.” Building accessible sites and apps is part design, part UX, part coding, and a whole lot of testing. 

In fact, accessibility is something that concerns all of us, every day. What we create is useless if it isn’t accessible. We don’t want technology to become a discriminator factor against people.

Currently around 10 per cent of the total world's population, or roughly **650 million people,** live with a disability.

**Recommended: Javascript and Civil Rights -> https://www.youtube.com/watch?v=hUCOA338jwk**

![Javascript and civil rights](images/javascript-civil-rights.jpg)

# Terms glossary
Having this terms in mind is going to help you a lot understand any documentation about accessibility:
 
- **A11y:** A shorter way to refer to “digital accessibility”
- **WAI:** Web Accessibility Initiative
- **WAI-ARIA:** a set of additional HTML attributes that can be applied to elements to provide additional semantics and improve accessibility wherever it is lacking. Very useful in one page applications. [More info](https://developer.mozilla.org/en-US/docs/Learn/Accessibility/WAI-ARIA_basics)
- **WCAG:** Web content accessibility guidelines
- **WCAG 2.0:** Current criteria for WCAG accesibility guidelines
    - **Level A:** Basic Accessibility features
    - **Level AA:** Most common user barriers
    - **Level AAA:** Highest & stronguest compliance of web accessibility.
- **Accessibility tree:** The browser modifies the DOM tree in order to generat a form useful for assistive tecnology. That’s what we call accessibility tree. [More info](https://developers.google.com/web/fundamentals/accessibility/semantics-builtin/the-accessibility-tree)
- **Progressive enhancement:** It is a strategy for web design that emphasizes core webpage content first. It ensures content is still accessible if CSS or Javascript are disabled. [More info](https://developer.mozilla.org/en-US/docs/Glossary/Progressive_Enhancement)



# Types of disabilities

### **Visual**

Example: blindness, low-vision, color blindness

- may use a screen reader to navigate their computers and mobile devices
- may use software and devices to increase the size of text and applications on the screen.
- may have a different perception of colors


### **Hearing**

Example: deaf and hard of hearing

- may need text alternatives for audio content


### **Motor**

Example: not having the use of certain limbs and paralysis

- They may not be able to use a mouse and instead perform all navigation via keyboard
- They could use dictation software.


### **Speech**

Example: people who are not able to speak or who have a speech impediment

- They are not able to use voice commands


### **Cognitive**

Example: dyslexia, autism, ADHD

- They may be able to use a mouse, a keyboard, and a monitor, but they may run into barriers with certain user interfaces or design components
- Moving, blinking, or flickering content, and background audio can be a problem.
- May need a way to suppress animations and audio.


**-> Check this poster series for quick tips on how to adapt interfaces to all kinds of disabilities [Github project](https://github.com/UKHomeOffice/posters/tree/master/accessibility/dos-donts)**

![Accessibility Posters](images/accesibility-posters.png)


# Compliance Level

The European Union is officially requiring its member states to abide by **WCAG 2.1 Level AA standards.**

This requirements includes:
- Developing
- Interaction design
- Content creation
- Visual design. 

We, as developers, use to work only on the developing phase but other parts are going to affect the final result and level of compliance.


**-> See full requirements:** https://www.w3.org/WAI/WCAG21/quickref/?currentsidebar=%23col_customize#top


Note: The points that we are going to cover on the next sections, focuses on level AA, leaving out some accessibility aspects like motion control.




# Coding for visual disabilities

## **Blind people: Screen readers**

These are the most common screen readers:
- JAWS (Windows)
- ZoomText (Mac and Windows)
- Window-Eyes (Windows)
- NVDA (Windows)
- VoiceOver (Mac)
- ChromeVox (Browser)


### **1. Reflect the reading order in the code order.**

### **2. Structure content using HTML5 semantic tags**. 

Using semantig tags createa landmarks that allows screen reader user to navigate between regions of the page.
-> See how they work

````HTML
<article>
<aside>
<details>
<figcaption>
<figure>
<footer>
<header>
<main>
<mark>
<nav>
<section>
<summary>
<time>
````

**-> to learn how to use them correctly check https://www.freecodecamp.org/news/semantic-html5-elements/**

**Pro tips:**
- This tags are only used to describe semantics. You can still use `<div>`for CSS/JS only purposes.
- Use the heading tags and don’t skip any level
- Use native HTML elements whenever is possible. They already provide the semantics and functionalities required. 
`<button>` `<form>` `<input>` `<text-area>`, etc.



### **3. Add labels to form elements:** 

Form input elements need linked ``<label>`` for screenreaders access . 

- The placeholder attribute is not an alternative for the label attribute because it disappears when interacting with the input field.
- Placing the label tag including the input tag allows the user to focus on the input when selecting the label via keyword.

````HTML
<!-- Bad: Use the placeholder as label -->
<input type="text" id="Name" autocomplete="family-name" placeholder="Name">

<!-- Bad: Place <input> outside <label> -->
<label>Name: </label>
<input type="text"  placeholder="Name">

<!-- Good: -->
<label>
	Name: 
	<input type="text"  placeholder="Name">
</label>
````


### 4. **Provide text for non-text content: **

- **Every content not represented by text is invisible for screen readers.**

  1. Provide concise descriptions in **alt attributes** for all content images and text alternatives for charts etc. 
  2. CAPTCHAs also need to be accessible.

    

- **Use WAI-ARIA tags for dynamic sites:**

   Dynamic sites such as single page applications tend to get complicated when it comes to accessibility. The WAI-ARIA standard provides tools to tackle accessibility in dynamic applications

   Check WAI-ARIA Section


### 5. Build for keyboard use only:
Check "Coding for keyword support"


### 6. Hide accesory content from screen readers or show specific content to ensure comprehension:

````CSS
/*Hiding things from screen readers*/
.class {
display: none;
visibility: hidden;
}
````
````HTML
<input hidden />
````

````CSS
/*Hide from user but show for screenreaders*/
.screenreader {
position: absolute;
left: -10000px;
width: 1px;
height: 1px;
overflow: hidden;
} 
````

## **People with low vision**

### 1. Allows users to zoom-in

````HTML
<!-- Bad: -->
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

<!-- Good: -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">
````




### 2. Allow users personalize the font-size

````CSS
<!-- Bad: -->
html {
    font-size: 16px;
}

<!-- Good: -->
html {
    font-size: 100%;
}
````


### 3. Ensure that previous actions doesn't break the layout

- Code everything with relative units (em, rem or %). 
- Avoid absolute positioning. 


### 4. Responsible use of color. 
- Check color contrast
- Don't rely on color as the only way of communicating meaning.


````CSS
/* Bad */
a {
   color: red;
   text-decoration: none;
}

/* Good */
a {
   color: red;
   text-decoration: underline; */ This way we are providing a second source of information for the user */
}
````

# Examples of accessible components
  
- [Basic page setup (WIP)](https://codepen.io/irene_mallafre/pen/bGNjVjz) 
 
- [Navbar: list style + aria labelling (WIP)](https://codepen.io/irene_mallafre/pen/xxbeNjG)
 
- [Toggle menu: list style + aria labelling (WIP)](https://codepen.io/irene_mallafre/pen/GRgeJvx)

- [Different navigation examples](https://a11y-style-guide.com/style-guide/section-navigation.html)
 
- [Cards](https://a11y-style-guide.com/style-guide/section-cards.html)
 
- [Forms](https://a11y-style-guide.com/style-guide/section-forms.html)


# Checklist for AA Compliance
 
Follow this checklist to ensure that you are following accessibility best practices. 
https://a11yproject.com/checklist/


# Testing for accessibility

1. Zoom to 200%
Use your browser or document viewer zoom. Does all text remain visible?
 
2. Check keyboard navigation
Navigate just using ←↑↓→ TAB, SHIFT+TAB, ESC, and ENTER keys.
 
3. Turn your monitor off and use a screen-reader
E.g. Apple Voiceover or Narrator on Windows
 
4. Deactivate CSS
Check if the document structure still makes sense.
 
5. Deactivate Images
Check if the content is still readable and understandable without the images
 
6. Run "Wave", an accessibility checker extension for Chrome.
https://chrome.google.com/webstore/detail/wave-evaluation-tool/jbbplnpkjmmeebjpijfedlgcdilocofh
 
7. Run Google Chrome accessibility audit
It’s in the "Audits" section inside of the DevTools.




