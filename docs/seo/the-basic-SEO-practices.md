# The Basic SEO practices we should implement on our website

## 1. Sitemap

Within the sitemap is where we define the URL's we want to be indexed by the search crawlers.  
Questions to ask yourself before the implementation:

- Does my site have dynamic creation of pages?
- What are my canonical pages? [+info](https://support.google.com/webmasters/answer/139066)
- Is my site localized by languages? [+info](https://support.google.com/webmasters/answer/189077)

[How to create a correct sitemap](https://support.google.com/webmasters/answer/183668)

## 2. Robots.txt

Within the robots.txt is where we can define rules that will specify/control the behaviour of the crawlers during the indexation.
Questions to ask yourself before the implementation:

- Does my site have duplicated content? [Avoid creating duplicate content](https://support.google.com/webmasters/answer/66359) [What is duplicate content?](https://moz.com/learn/seo/duplicate-content)
- Are there certain pages I don't want to be indexed?

[How to create a correct robot.txt](https://support.google.com/webmasters/answer/6062596)

## 3. Friendly URL's

Our structure of the URL's should follow some guidelines to be indexation friendly and to avoid the indexation of duplicated content.  
[How to structure our url's to be friendly with the user and the crawler](https://support.google.com/webmasters/answer/76329)

## 4. Clear conceptual page hierarchy design

We should structure our page content as sections that describe a schematic hierarchy of importance.  
We should use the Header tags descending from h1 to h6.
Since HTML5, there are new tags that allow us to create sections explicitly.

[Structure content using HTML5 semantic tags.](/docs/accessibility/accessibility.md#2-structure-content-using-html5-semantic-tags)

Their use enhances the expressiveness of the structure of our content and increases accessibility.  
[How schematization algorithm works](https://developer.mozilla.org/es/docs/Sections_and_Outlines_of_an_HTML5_document#El_algoritmo_de_esquematizado_de_HTML5)

## 5. Tag Title and Metatag Description

Both must be added within the `<head></head>`

- `<title></title>` A title tag is an HTML element that specifies the title of a web page. Title tags are displayed on search engine results pages (SERPs) as the clickable headline for a given result, and are important for usability, SEO, and social sharing. The title tag of a web page is meant to be an accurate and concise description of a page's content.  
  [Best practices for title tag](https://moz.com/learn/seo/title-tag)
- `<meta name="description"><meta>` The meta description is an HTML attribute that provides a brief summary of a web page. Search engines such as Google often display the meta description in search results, which can influence click-through rates.  
  [Best practices for meta tag description](https://moz.com/learn/seo/meta-description)

## 6. Alt text

We must implement the alt text in all images of the page. These are used to describe the appearance and function of an image on a page.
`<img alt="alt text here">`  
[How to write a good alt text](https://moz.com/learn/seo/alt-text)

## 7. Canonicalization

Must be added within the `<head></head>`  
`<link rel="canonical" href="The canonical URL goes here">` What is a canonical tag? A canonical tag is a way of telling search engines that a specific URL represents the master copy of a page. Using the canonical tag prevents problems caused by identical or "duplicate" content appearing on multiple URLs. Practically speaking, the canonical tag tells search engines which version of a URL you want to appear in search results.  
[Best practices for Canonical tag](https://moz.com/learn/seo/canonicalization)

## 8. HTML lang attribute

By default, the html sets a lang attribute on html tag `<html lang="es"></html>`.
There're rumours that crawlers don't take this attribute as important, although it is best practice to always have this attribute correctly localized.

## 9. Google Search Console

We must link our page to [Google Search Console](https://search.google.com/search-console)
Google Search Console offers features like:

- Force the indexation of our sitemap
- Recieve feedback of our indexed pages
- Receive tips for SEO best practices
- Be able to view what the crawler have actually indexed

# Some good tools/pages to maintain the SEO

- [Lighthouse](https://developers.google.com/web/tools/lighthouse/)
- [Pagespeed Insights](https://developers.google.com/speed/pagespeed/insights/)
- [Page speed](https://developers.google.com/speed)
- [Rich results](https://search.google.com/test/rich-results)

# OK but...What happens with all that if my site is an SPA?

Don't worry. I don't know about other crawlers but google bot is currenly indexing with a process that waits until all javascript has finished rendering. That allow us to dynamically change the root html.  
The final assurance that the crawler is indexing what we actually want is very easy to check with tools like [Google Search Console](https://search.google.com/search-console) or [Google Rich Results](https://search.google.com/test/rich-results)  
There are other approaches like SSR, Dynamic Rendering...but they go out of the scope of this topic.
