# Development environment setup

The development environment must provide all required system tools to work on
any frontend project

The purpose of this document is to declare all required tools and provide
instructions on how to install them

- Package manager
- [Git](https://git-scm.com/)
- [Node.js](https://nodejs.org/en/)
- [Visual Studio Code](https://code.visualstudio.com/)
- [Docker](https://www.docker.com/)

## Package Manager

It is recommended to install all system tools through a package manager

**Note:** All system packages must be installed using a terminal with
administrative privileges

### Linux installation

All linux distributions already include an official package manager

- **Debian/Ubuntu/Mint:** `apt`
- **ArchLinux/Manjaro:** `pacman`

### Windows installation

Windows users are encouraged to use the
[Chocolatey Package Manager](https://chocolatey.org/)

**From a `cmd.exe` with administrative privileges:**

```
@"%SystemRoot%\System32\WindowsPowerShell\v1.0\powershell.exe" -NoProfile -InputFormat None -ExecutionPolicy Bypass -Command "iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))" && SET "PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin"
```

It is recommended to configure Chocolatey to allow global installations without
confirmation by default

```
choco feature enable -n allowGlobalConfirmation
```

### MacOS installation

MacOS users are encouraged to use the
[Homebrew Package Manager](https://brew.sh/)

**From a `bash` shell:**

```bash
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

## Git

Git is a distributed version control system

Git provides the `git` system tool

### Windows installation (Chocolatey)

```
choco install git
```

[Git For Windows](https://gitforwindows.org/) provides the following additional
tools:

- `git-bash`: A `bash` emulation used to run Git from the command line
- `git-gui`: A graphical version of just about every Git command line function,
  as well as comprehensive visual diff tools

## Node.js

Node.js is a JavaScript runtime built on Chrome's V8 JavaScript engine.

Node.js provides the following system tools:

- `node`: A JavaScript runtime
- `npm`: Node Package Manager
- `npx`: Execute locally installed npm packages from the terminal

### Windows installation (Chocolatey)

```
choco install nodejs
```

## Visual Studio Code

Visual Studio Code is a source code editor with built-in support for JavaScript,
TypeScript and Node.js

Visual Studio Code provides the `vscode` system tool

### Windows installation (Chocolatey)

```
choco install vscode
```

### Extensions

- [EditorConfig](https://marketplace.visualstudio.com/items?itemName=EditorConfig.EditorConfig)
- [ESLint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)
- [Stylelint](https://marketplace.visualstudio.com/items?itemName=stylelint.vscode-stylelint)
- [Prettier](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)

## Docker

Docker provides container software that is ideal for developers and teams
looking to get started and experimenting with container-based applications

Docker provides the `docker` system tool

### Windows installation (Chocolatey)

```
choco install docker-desktop
```
