# Tech Stack

The `angular-starter` project is powered by lots of open source tools with great
community adoption. Checkout its documentation

## JavaScript Transpiler: TypeScript

- [TypeScript](https://www.typescriptlang.org/)
- [TypeScript Deep Dive](https://basarat.gitbooks.io/typescript/content/)

## CSS Preprocessor: Sass

- [Sass](https://sass-lang.com/)
- [Sass Guidelines](https://sass-guidelin.es/)

## SPA Framework: Angular

- [Angular Docs](https://angular.io/docs)
- [Angular CLI](https://github.com/angular/angular-cli)
- [Angular Router](https://angular.io/guide/router)

## UI Components: Bootstrap

- [Bootstrap](https://getbootstrap.com/)
- [NgBootstrap](https://ng-bootstrap.github.io/#/home)

## Unit testing

- [Karma](https://karma-runner.github.io)
- [Jasmine](https://jasmine.github.io/)
- [Headless Chrome](https://developers.google.com/web/updates/2017/04/headless-chrome)

## Backend mock

- [Express](https://expressjs.com/)
- [LowDB](https://github.com/typicode/lowdb)

## CI

- [Docker](https://docs.docker.com/)
- [Nginx](http://nginx.org/en/docs/)

## Component-Driven Development

- [Storybook](https://storybook.js.org/basics/guide-angular/)
- [Learn Storybook](https://www.learnstorybook.com/angular/en/get-started)
