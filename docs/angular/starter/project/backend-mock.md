# Backend Mock

Every frontend application needs a data provider to consume data from

It usualy happens that some parts of the API are not implemented yet while the
frontend developers are working on related features

Even when the API is ready, for the frontend development team it can be
cumbersome to setup, launch and keep updated the backend server and having a
real authentication layer in the server makes the application components harder
to test

Frontend and backend development should be driven by API contract. Having an
API contract available for both teams allows parallel development by minimizing
its inner dependencies

**The backend mock is a fake backend server for frontend development**

It allows the frontend team to focus on its own backlog while having a great
testing environment

The Angular application is totally decoupled from the backend mock. It should be
configured to make requests to the backend mock only in the development
environment and to make requests to the real backend in all other environments

This settings can be defined in the environment configuration files under the
`src/environments/` folder

## Usage

The backend mock is launched by PM2 along with the other development servers

```
npm start
```

The backend mock is an `express`-based server with a `lowdb` JSON database

When a new feature requirement arrives that needs an endpoint to work with, it
is recommended to create a fake endpoint first, then work on the new feature by
attacking to the fake endpoint and finally test the implementation against the
real backend in the integration phase

## References

- [express](https://expressjs.com/)
- [lowdb](https://github.com/typicode/lowdb)
