# Setup

![upstream-repo](/images/upstream-repo.png)

## Initialization

### Local clone

Clone the project repository to your local environment

```
git clone <project_url>
```

The `git clone` command initializes your local repository with a remote called
`origin` pointing to the URL where you have cloned the repo from

### Add `upstream` remote

The **upstream repository** is the central repository for the `angular-starter`
project

To update the project with the latest changes from `angular-starter` you need to
add the angular-starter as upstream remote in your local repository

```
cd <project_root>
git remote add upstream https://steps.everis.com/git/DEVFORIND/angular-starter.git
```

### Install dependencies

Install project dependencies from the npm registry

```
cd <project_root>
git checkout develop
npm install
```

The `npm install` command installs all dependencies specified in the
`package.json` and save them at `node_modules/`

## Updates

In order to keep your code up to date with the community you want to update each
aspect of your project:

- **Team development:** To minify merge conflicts, developers will often get
  changes from `origin/develop`
- **Architecture:** Initial architecture comes from the `angular-starter`
  project
- **Project dependencies:** Node packages are managed with the `npm` command

Each aspect has its own strategies and timings to have in consideration

### Integrate team development

From your current branch, get updates from `origin/develop`

```
cd <project_root>
git pull origin develop
```

The more often you get changes from `develop` the lesser conflicts in your
merges

### Update architecture

It is recommended to create a new feature branch for adding architecture updates

```
cd <project_root>
git checkout develop
git checkout -b feature/upstream-sync
```

Architecture updates must be done from realease tags (e.g. `v1.3.2`) of the
`upstream` remote

```
git pull upstream <release_tag>
```

To prevent updates with breaking compatibility changes, be aware to only get
updates from releases with the same `MAJOR` version as the one that was used to
generate the project.

You can find the starter version you based your project on in the package.json´s
property `starter-version`.

**IMPORTANT:** you should never change its value. That value will be updated in
the starter project each time there is a release. Use the package.json´s
property `version` instead to refer to your project version.

Once the changes are merged and successfully tested you can proceed to integrate
the branch into `develop`.

```
git checkout develop
git merge feature/upstream-sync
git push
```

### Update project dependencies

```
cd <project_root>
npm update
```

Be aware of [version ranges](https://steps.everis.com/git/DEVFORIND/angular-starter/wikis/methodology/semantic-versioning#version-ranges)
being specified as follows:

- **For development:** Use caret ( `^`)

  Caret is the default range used by npm when you `npm install` a package. It
  lets us get **bugfixes and new features** which is recommended during
  development

- **For maintainance:** Use tilde (`~`)

  In maintainance mode, it is recommended to minimize the dependency updates
  by getting **only bugfixes**

- **Before delivery:** Fix versions

  Before transfering project ownership it is recommended to replace all
  version ranges specified in `package.json` (i.e. `^1.2.3`, `~1.2.3`) by
  fixed versions (e.g. `1.2.3`) in both `dependencies` and `devDependencies`
