# Folder Structure

```
[-] <root>/
 |- [+] node_modules/
 |- [-] src/
 |   |- [+] api/
 |   |- [-] app/
 |   |   |- [+] core/
 |   |   |- [+] shared/
 |   |   |- [+] <feature>/
 |   |- [+] assets/
 |   |- [+] environments/
 |   |- [+] stories/
 |   |- [+] styles/
```

## Directories

### Root

```
[-] <root>/
 |- [+] node_modules/
 |- [+] src/
```

The `<root>` folder is where you have cloned the project. It contains
configuration files for the tools used in the project such as `Git`, `NodeJS`,
`Angular CLI` and `TypeScript`

- `node_modules/`: Provides npm packages to the project
- `src/`: Contains source files (app logic, data, and assets), along with
  configuration files for the initial app

### `src/`

```
[-] src/
 |- [+] api/
 |- [+] app/
 |- [+] assets/
 |- [+] environments/
 |- [+] stories/
 |- [+] styles/
```

The `src/` folder contains all required files to build the application artifacts
that will be deployed to the different environments such as the entry points
`index.html`, `main.ts` and `styles.scss`

- `api/`: Backend mock for development purposes
- `app/`: Contains the component files in which your app logic and data are
  defined
- `assets/`: Contains image files and other asset files to be copied as-is
  when you build your application
- `environments/`: Contains build configuration options for particular target
  environments
- `styles/`: Contains common Sass helpers, global styles and themes

### `src/app/`

```
[-] src/app/
 |- [+] core/
 |- [+] shared/
 |- [+] <feature>/
```

The `app/` folder contains the files that compose the Angular app and its unit
tests, including component files and services. Here is where the work is usually
done

- `core/`: Contains commonly used singleton services available for use in many
  other modules
- `shared/`: Contains commonly used components, directives and pipes available
  for use in templates of components in other modules
- `<feature>/`: Create your own feature areas with its own directory and
  `NgModule`

## Files

### Root

Contains configuration files for the tools used in the project

```
[-] <root>/
 |- [+] node_modules/
 |- [+] src/
 |- .editorconfig
 |- .gitignore
 |- angular.json
 |- package.json
 |- package-lock.json
 |- tsconfig.json
 |- tslint.json
 |- README.md
```

- `.editorconfig`: Configuration for code editors
- `.gitignore`: Specifies intentionally untracked files that Git should ignore
- `angular.json`: Angular CLI configuration defaults including configuration
  options for build, serve, and test tools
- `package.json`: Configures npm package dependencies for the project
- `package-lock.json`: Provides version information for all packages installed
  into node_modules by the npm client
- `tsconfig.json`: Default TypeScript configuration including TypeScript and
  Angular template compiler options
- `tslint.json`: Default TSLint configuration
- `README.md`: Introductory documentation

### `src/`

Contains source files (app logic, data, and assets), along with configuration
files for the initial app

Files at the top level of `src/` support testing and running your app

```
[-] src/
 |- [+] api/
 |- [+] app/
 |- [+] assets/
 |- [+] environments/
 |- [+] stories/
 |- [+] styles/
 |- browserlist
 |- favicon.ico
 |- index.html
 |- main.ts
 |- polyfills.ts
 |- styles.scss
 |- test.ts
 |- tsconfig.app.json
 |- tsconfig.spec.json
 |- tslint.json
```

- `browserlist`: Configures sharing of target browsers and Node.js versions
  among various front-end tools
- `favicon.ico`: An icon to use for this app in the bookmark bar
- `index.html`: The main HTML page. The CLI automatically adds all JavaScript
  and CSS files when building your app
- `main.ts`: The main entry point for your app. Compiles the application with
  the JIT compiler and bootstraps the application's root module (AppModule) to
  run in the browser
- `polyfills.ts`: Provides polyfill scripts for browser support
- `styles.scss`: Lists CSS files that supply styles for a project. The
  extension reflects the preprocessor you have configured for the project
- `test.ts`: The main entry point for your unit tests, with Angular-specific
  configuration
- `tsconfig.app.json`: Inherits from the workspace-wide `tsconfig.json` file
- `tsconfig.spec.json`: Inherits from the workspace-wide `tsconfig.json` file
- `tslint.json`: Inherits from the workspace-wide `tslint.json` file

### `src/app/`

The `app/` folder contains your app's logic and data. Angular components,
templates, and styles go here

```
[-] src/app/
 |- [+] core/
 |- [+] shared/
 |- app.component.ts
 |- app.component.html
 |- app.component.scss
 |- app.component.spec.ts
 |- app.module.ts
```

- `app.component.ts`: Defines the logic for the app's root component, named
  `AppComponent`
- `app.component.html`: Defines the HTML template associated with the root
  component
- `app.component.scss`: Defines the base CSS stylesheet for the root component
- `app.component.spec.ts`: Defines a unit test for the root component
- `app.module.ts`: Defines the root module, named `AppModule`, that tells
  Angular how to assemble the application

## References

- <https://angular.io/guide/styleguide#application-structure-and-ngmodules>
- <https://angular.io/guide/file-structure>
