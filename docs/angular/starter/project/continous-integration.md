# Continous Integration

**Ngine** comes with CI support through `docker` and `nginx`

## Docker container

There is a `Dockerfile` in the root folder which should be used in CI
environments to build the container from

The container will serve an static application using an `nginx` server thus
it expects to have a successfuly built app in the `dist/ng-starter/` folder

The `nginx.conf` configuration file is also available in the root folder as
defined in the `Dockerfile`

```bash
cd <project_root>
npm run build
docker build -f ./Dockerfile -t app .
```

You can start the container's app server with the following command:

```bash
docker run -d app
```

## Nginx server

The Nginx server comes with the minimal configuration to serve an Angular
application

It serves the application from `http://localhost:8080` which won't be an
exposed server so it can have an exposed load balancer or proxy which will
redirect requests to it

```nginx
listen 8080;
```

### URL matching

It handles any application URL by serving the root Angular application so the
application can handle its own routes

```nginx
location / {
    root   /opt/app;
    try_files $uri $uri/ /index.html;
}
```

### Backend proxy

Having control of the application server allows us to configure the backend API
contexts as desired, thus all API requests done by the Angular app can have a
clean `/api` context and preventing the need of using CORS and its pre-flight
requests (`OPTIONS` HTTP requests)

By default, the Nginx configuration doesn't have a proxy. It will be a task to
do for each project based on the backend API URLs of the project

```nginx
location /api/stock-manager/ {
    proxy_pass http://scoa-stock-manager:8001/stock-manager/;
    proxy_set_header Host $host;
    proxy_set_header X-Real-IP $remote_addr;
}

location /api/ingress-service/ {
    proxy_pass http://scoa-ingress-service:8002/ingress-service/;
    proxy_set_header Host $host;
    proxy_set_header X-Real-IP $remote_addr;
}
```

## References

- [docker](https://docs.docker.com/)
- [nginx](http://nginx.org/en/docs/)
