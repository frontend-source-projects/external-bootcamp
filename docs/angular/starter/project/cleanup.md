# Cleanup

**Ngine** comes with a **demo** feature as example of implementation. It
is recommended to cleanup all pre-generated demo contents before the project
starts

1.  `src/app/views/views-routing.module.ts`

    - Remove the `DemoComponent` import
    - Remove the `demo` route

2.  `src/app/viws/views.module.ts`

    - Remove the `DemoModule` import
    - Remove the `DemoModule` from module `imports`

3.  Delete the **demo** feture area at `src/app/views/demo/`
4.  Remove the **demo**-related endpoints at `src/api/api.js`
5.  Remove the **demo**-related entities at `src/api/db.json`
6.  Remove the **demo** link and the cleanup instructions at
    `src/app/views/landing/landing.component.html`
