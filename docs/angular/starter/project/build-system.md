# Build System

The build system consist of a set of task scripts designed to perform the
required actions to build and test the application

All tasks are defined in the `scripts` section of the `package.json`

**package.json**

```json
{
  "scripts": {
    "start": "ng serve",
    "test": "ng test --watch=false",
    "coverage": "http-server ./coverage -p 4300 --silent"
  }
}
```

In the example above, there are 3 tasks defined: `start`, `test` and `coverage`

Use `npm run <task>` to execute a task. There are some tasks which can be
executed with a shorter syntax, such as `start` or `test`

```
npm run start     # executes the `start` task
npm start         # same as above with shorter syntax

npm run coverage  # executes the `coverage` task
npm coverage      # raises an error. `coverage` has no shorter syntax available
```

The commands with shorter syntax are those predefined by npm, such as `start`,
`stop` and `test`. For a complete list of predefined commands, visit the npm
official documentation: <https://docs.npmjs.com/misc/scripts>

## Tasks

### Development servers

```
npm start
```

Launches all development servers configured in the PM2 `ecosystem.config.js`
config file

- `app` server: Angular app server (`ng serve`)
- `api` server: Backend mock server (`express`)
- `proxy` server: Proxy for XHR requests (`express` + `http-proxy-middleware`)

```
npm stop
```

Stops all development servers started with `npm start`

```
npm restart
```

Restarts all servers by executing `npm stop` followed by `npm start`

```
npm run logs
```

Show and follow the logs for all development servers

Use `Ctrl+C` to stop following the logs

```
npm run logs <server_name>
```

Show and follow the logs for the selected server

Use `Ctrl+C` to stop following the logs

### Build

```
npm run build
```

Builds the project. The build artifacts will be stored in the `dist/` directory.
Use the `--prod` flag for a production build

### Unit tests

```
npm test
```

Runs unit tests written with **Jasmine** using a Headless Chrome instance via
**Karma**. The tests will be executed automatically if you change any of the
source or test files

### Coverage

```
npm run start:coverage
```

Starts a static server pointing to the **Istanbul** HTML coverage report

### Update Angular dependencies

```
npm run update:app
```

Updates the angular dependencies and its versions specified at `package.json`
