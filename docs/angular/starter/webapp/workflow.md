# Workflow

To create a new feature you should proceed as follows:

1.  Create a **database entity**
2.  Create an **endpoint** for the entity
3.  Create an **interface** for the entity
4.  Create an Angular **service** connected to the endpoint
5.  Create an Angular **component** using the service (or use an existing
    component to add the new functionality)

## Creating a database entity

A database entity is an entry in the `db.json` file. It contains the state
persisted on disk

**src/api/db.json**

```json
{
  "books": [
    { "title": "Eloquent JavaScript", "author": "Marijn Haverbeke" },
    { "title": "JavaScript: Good Parts", "author": "Douglas Crockford" },
    { "title": "Async JavaScript", "author": "Trevor Burnham" },
    { "title": "JavaScript Patterns", "author": "Stoyan Stefanov" }
  ]
}
```

## Creating an endpoint

An endpoint is composed by an `express` route with a `lowdb` database access.
It performs CRUD (create, read, update and delete) operations on the database
entities

**src/api/api.js**

```js
app.get("/api/books", function(req, res) {
  const books = db.get("books").value();
  res.status(200).send(books);
});
```

## Creating an interface

An interface is a TypeScript `interface` declaration of the database entity. It
provides type annotations for Angular services and components

**src/app/books/books.model.ts**

```ts
export interface Book {
  title: string;
  author: string;
}
```

## Creating a service

An Angular service is a data provider for components. It makes the needed `HTTP`
requests to the backend and returns an `Observable` wrapping the server response

**src/app/books/books.service.ts**

```ts
export class BooksService {
  constructor(private http: HttpClient) {}

  getBooks(): Observable<Book[]> {
    return this.http.get<Book[]>("http://localhost:3000/api/books");
  }
}
```

## Creating a component (or using an existing one)

An Angular component is a data provider for the HTML template. It subscribes
itself to the Observable provided by the service and stores the result in its
internal state

Alternatively to creating a new Angular component you can extend an existing one
with the new functionality. The following example shows how to use the newly
created service from a component, whether it is new or an existing one

**src/app/books/books.component.ts**

```ts
export class BooksComponent implements OnInit {
  books: Book[];
  constructor(private booksService: BooksService) {}
  ngOnInit() {
    this.getBooks();
  }

  getBooks() {
    this.booksService.getBooks().subscribe(books => {
      this.books = books;
    });
  }
}
```

An HTML template references the internal component state

**src/app/books/books.component.html**

```html
<h1>Books</h1>

<ul>
  <li *ngFor="let book of books">{{ book.title }} - {{ book.author }}</li>
</ul>
```
