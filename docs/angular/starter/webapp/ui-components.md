# UI components

There are several well-supported opensource CSS component libraries to choose
from. **Ngine** doesn't provide a default one because its easier to
choose which is best suited for the project when there are UX designs available

The documentation below describes step by step how to install those CSS
component libraries which we think are the most frequently used in Angular apps:

- `NgBootstrap`
- `Angular Material`

## `NgBootstrap`

`Bootstrap` comes with both CSS components and JavaScript sources to provide
user interactivity to some of those components

The `Bootstrap` JavaScript is based on `JQuery`. Since we are using Angular it
is best suited to use `NgBootstrap` instead of the builtin `JQuery` extensions

`NgBootstrap` uses `Bootstrap`'s CSS so we need to install `Bootstrap` first

To enable all bootstrap features like class extensions and theming we use the
Sass sources from Bootstrap

### Bootstrap installation

Install the full bootstrap package

```
npm install bootstrap
```

Import its main Sass file in your `src/styles/_modules.scss`

```scss
@import "~node_modules/bootstrap/scss/bootstrap";
```

### NgBootstrap installation

```
npm install @ng-bootstrap/ng-bootstrap
```

Add the `NgbModule` to your `SharedModule`

**src/app/shared/shared.module.ts:**

```ts
import { NgModule } from "@angular/core";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";

@NgModule({
  imports: [NgbModule],
  exports: [NgbModule]
})
export class SharedModule {}
```

**Note:** Beware to import the `SharedModule` from every module having a
component which uses `NgbBootstrap` functionality

## Angular Material

Angular Material is maintained by the Angular team

### Installation

Install required dependencies

**Note:** Beware to install the version matching your version of Angular

```
npm install --save @angular/material @angular/cdk @angular/animations
```

### Enable animations support

Import `BrowserAnimationsModule` into your application to enable animations
support

**src/app/app.module.ts**

```ts
import { NgModule } from "@angular/core";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

@NgModule({
  imports: [BrowserAnimationsModule]
})
export class AppModule {}
```

### `MaterialModule`

Create a `MaterialModule` inside the `shared/` feature area with all required
modules from Angular Material

**src/app/shared/material.module.ts**

```ts
import { NgModule } from "@angular/core";
import { MatButtonModule, MatCheckboxModule } from "@angular/material";

@NgModule({
  imports: [MatButtonModule, MatCheckboxModule],
  exports: [MatButtonModule, MatCheckboxModule]
})
export class MaterialModule {}
```

### Load a theme

Including a theme is **required** to apply all of the core and theme styles to
your application

**src/styles/\_theme.scss**

```scss
@import "~@angular/material/theming";

@include mat-core();

$app-primary: mat-palette($mat-indigo);
$app-accent: mat-palette($mat-pink, A200, A100, A400);
$app-theme: mat-light-theme($app-primary, $app-accent);

@include angular-material-theme($app-theme);
```

### Add a CSS resets library

Unlike Bootstrap, Angular Material doesn't come with a default CSS resets

Install `normalize.css`

```
npm install normalize.css
```

Load the CSS resets library from `angular.json` `build` and `test` targets

```json
{
  "styles": ["node_modules/normalize.css/normalize.css", "src/styles/main.scss"]
}
```

## References

### NgBootstrap

- <https://getbootstrap.com/docs/4.3/getting-started/introduction/>
- <https://ng-bootstrap.github.io/#/home>

### Angular Material

- <https://material.angular.io/guide/getting-started>
- <https://material.angular.io/guide/theming>
