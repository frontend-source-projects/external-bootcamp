# Itinerario Formativo Angular

![Angular](https://angular.io/assets/images/logos/angular/angular.svg)

## ¿Qué vas a aprender?

Esta guía va a proporcionarte las herramientas para que comprendas las bases de Angular y seas capaz de desarrollar un proyecto sencillo en este framework.

Asi como programar en typescript que es un lenguaje que extiende javascript y te permite tiparlo entro otras cosas.

Es muy importante que comprendas muy bien las bases del framework y el lenguaje para que más adelante no te supongan un bloqueo en tu día a día, así que sigue paso a paso la guía y no tengas prisa por llegar al final.

## Typescript (Tiempo estimado para mirarlo 8h)

¿Qué es typescript? :)

TypeScript es un lenguaje de programación libre y de código abierto desarrollado y mantenido por Microsoft. Es un superconjunto de JavaScript, que esencialmente añade tipos estáticos y objetos basados en clases.

¿Por qué usar TypeScript?

Como decíamos antes, TypeScript es un superconjunto de JavaScript, coge este lenguaje y lo mejora con nuevas funcionalidades. Uno de los principales añadidos es que TS es de tipado fuerte. Esto supone un cambio importante en la manera de programar, puesto que con TypeScript puedes establecer explícitamente el tipo de cada variable o función.

Esto nos ofrece la seguridad de que el código funciona tal y como nosotros queremos: una variable tipada como number nunca podrá almacenar, por ejemplo, una cadena. Con esto reducimos las probabilidades de errores difíciles de detectar.

Pero además de hacer nuestro código más seguro, nos facilita consumir funciones desarrolladas por terceros. Muchas veces necesitamos saber qué tipo de valores espera una función que no hemos escrito nosotros, y tenemos que irnos a leer la documentación o mirar su código para saberlo. Los editores que soportan TypeScript nos ayudarán en esta tarea, y cuando escribamos el nombre de la función, automáticamente nos mostrarán los tipos de los argumentos que espera recibir y también el del valor que devolverá.

[Guía Rápida TypeScript](https://www.typescriptlang.org/docs/handbook/typescript-in-5-minutes.html)

[TypeScript y la Programación Funcional](https://www.typescriptlang.org/docs/handbook/typescript-in-5-minutes-func.html)

## ¿Qué es Angular y como funciona?

![Angular](img/What-is-angular.png)

[What is Angular?] (https://www.youtube.com/watch?v=VAkio68d51A)

## Mi primer proyecto (Tiempo estimado para acabarlo 40h)

[Desarrolla la aplicacion "Tour of heroes" en Angular](https://angular.io/tutorial)

## Ejercicio (Tiempo estimado para acabarlo 40h)

## ¿Qué vamos a hacer?

Como ejercicio final, para que pongas en práctica todos los conceptos anteriores, vamos a desarrollar una web de venta online de videojuegos.

## Objetivos

1. Dominar los conceptos básicos de Angular
2. Construir un componente
3. Entender como funciona el angular router para generar rutas

## Diseño:

El diseño que vamos a implementar lo vas a encontrar en la herramienta Zeplin.
Esta herramienta se usa habitualmente para traspasar diseños a desarrollo y como verás te permite inspeccionar los diseños y ver los estilos que se le aplican.

El ejercicio consisten en generar una vista de maestro de videojuegos y luego poder ir al detalle de cada uno.

**Descárgate el diseño([https://zpl.io/awvyzyK])**
Deberás solicitar el acceso al diseño primero.

## Metodología

Para desarrollar el producto, no vamos a hablar de tareas, si no de User Stories, reflejadas en un backlog.

[¿Qué son las user stories?](https://www.atlassian.com/es/agile/project-management/user-stories)

## ¡Empezamos!

### Set up del proyecto

Descárgate el starter oficial de Angular y levanta tu aplicación.

### Backlog

**UserStory-001**

Como usuario de la aplicación quiero poder visualizar una lista de videojuegos para poder ver el catálogo completo que me ofrecen y elegir al que quiero jugar.

![cardlist](img/Listado.png)

**Acceptance critera**

- La card contiene una imágen, el título del juego, el preció y un botón "Buy Now"
- El listado no es responsive
- El listado muestra 16 videojuegos, en 4 columnas.

**Tips**

- Crea los componentes que veas necesarios, por ejemplo, una card
- ¿Hay algún otro elemento que podamos considerar un componente y desarrollar por separado?
- Inspecciona los estilos en zeplin e intenta reproducirlos, para ser fiel a lo que quiere el cliente.

- Una vez que ya tenemos los componentes necesarios, queremos pintar en la home un listado dinámico de todos los videojuegos de los que disponemos en la plataforma.
- Ten en cuenta que la lista tendrá datos distintos. Pasar los datos por props te ayudará en este objetivo.

**UserStory-002**

Como usuario de la aplicación, quiero poder hacer click en un videojuego y visualizar su detalle para poder decidir si me interesa el juego.

![Detail](img/Vistadetalle.png)

**Acceptance critera**

- La vista detalle contiene:
  - Imágen principal
  - Etiqueta "New game"
  - Titulo del juego
  - Precio
  - Botón "Install game"
  - Descripción
  - Imágen

**Tips**

- Detecta el id de la card seleccionada por la url para pintar el detalle. Quizás la librería de Angular-douter-dom te pueda ayudar

**UserStory-003 ¡Extra!**

Como usuario de la aplicación, quiero poder ver al entrar el videojuego destacado, para estar informado de las novedades y tendencias.

![Detail](img/Carrousel.png)

**Acceptance critera**

- El banner se situará por encima del listado
- El banner ocupa un ancho del 100%
- Contiene:
  - Imágen
  - Título del videojuego
  - Breve descripción
  - Botón Install Game y Add to favourites (sin ninguna acción asociada)

**Tips**

- Detecta el id de la card seleccionada por la url para pintar el detalle. Quizás el modulo de routing de angular te pueda ayudar.
- Todas las agrupaciones de funcionalidades son componentes por ejemplo : una card es un componente 
- ¿Te atreves? -> Convierte el destacado en un carrousel.

---

**Recuerda hacer uso de todas las herramientas de desarrollo que tenemos. Lo ideal es que todo esté tipado y testeado.**

## Amplia tus conocimientos

### Routing

Qué es el routing? :)

¿Qué es enrutamiento estático?

Este es el enrutamiento más común, si alguna vez hemos trabajado con rutas en algún otro lenguaje de programación o con algún otro framework, seguramente lo habremos hecho usando enrutamiento estático.
En este, las rutas son definidas al momento en que nuestra aplicación es inicializada. Es decir, antes que nuestra aplicación se renderice.

¿Qué es enrutamiento dinámico?

En una SPA cambiamos lo que el usuario visualiza mostrando o escondiendo partes de la aplicacion. Estas partes corresponden a los distintos componentes y es esto lo que visualizamos en vez de ir a pedirle al servidor las paginas que queremos ver. Para gestionar la navegacion de una vista a otra se utiliza el Angular router. El router habilita la navegacion interpretando la url en el navegador como si fuese una instruccion para hacer un cambio de vista.

En D4i utilizamos el propio modulo de routing de angular [Angular Router](https://angular.io/guide/router).

### Arquitectura Angular

Overview de la aruitectura de angular [Arquitectura general de angular] (https://medium.com/angular-chile/angular-arquitectura-del-framework-e46204f38fef#:~:text=Angular%20es%20una%20plataforma%20y,aplicaciones%2C%20y%20otras%20son%20opcionales.)

### Unit testing

[Angular with karma](https://zeroesandones.medium.com/how-to-unit-test-your-angular-9-application-using-karma-testing-framework-o-6eefb743bca7)

## Recursos extra

### Editores de código online

Son herramientas de creación de prototipos e IDE instantánea para un rápido desarrollo web, donde tienen diferentes plantillas, que nos van a servir para crear proyectos de manera rápida online.

[Stackblitz](https://stackblitz.com/)

[CodeSandbox](https://codesandbox.io/)
