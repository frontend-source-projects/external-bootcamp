# Knowledge Base

## Getting Started

- [Development environment setup](docs/getting-started/development-environment.md)

## Methodology

- [Semantic versioning](docs/methodology/semantic-versioning.md)
- [Changelog convention](docs/methodology/changelog-convention.md)
- [Git hooks](docs/methodology/git-hooks.md)
- [Docker](docs/methodology/docker.md)

## Browsers

- [Browsers Compatibility and Known Issues](docs/browsers/browsers-compatibility.md)

## Angular

- [Angular Starter](docs/angular/starter/README.md)

## React

- [Style Guide](docs/react/style-guide/README.md)
- [Redux Epics](docs/react/redux-epics/README.md)
- [Material-UI](docs/react/Material-UI-React.md)
- [Formik](docs/react/formik/README.md)
- [Libraries inventory](docs/react/React-library-inventory.md)

## CSS / Sass

- [Naming classes: using BEM](docs/css/BEM-naming-clases.md)
- [Ordering your declaration blocks](docs/css/css-declaration-block-order.md)
- [Layout best practices](docs/css/layout-best-practices.md)
- [Breakpoints](docs/css/breakpoints.md)

## Testing

- [Unit Testing](docs/testing/unit-testing.md)
- [What to test](docs/testing/what-to-test.md)
- [Test without `require()`](docs/testing/test-without-require.md)

## Accessibility

- [Accessibility Basics](docs/accessibility/accessibility.md)

## SEO

- [The basic SEO practices](docs/seo/the-basic-SEO-practices.md)

## Formative paths

- [React](docs/formative-paths/react/Itinerario-formativo.md)
- [Angular](docs/formative-paths/angular/Itinerario-formativo.md)
